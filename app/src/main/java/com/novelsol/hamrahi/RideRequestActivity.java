package com.novelsol.hamrahi;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.novelsol.hamrahi.Adapters.RidesAdapter;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.PostInfo;
import com.novelsol.hamrahi.Model.Ride;
import com.novelsol.hamrahi.ui.DriverPostExtraInfo;
import com.novelsol.hamrahi.ui.MyTextView;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;
import com.novelsol.hamrahi.ui.RadiusInputDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Shahzaib on 7/16/2017.
 */

public class RideRequestActivity extends AppCompatActivity implements HTTP.RequestCompleteCallback, RadiusInputDialog.OnLocationSelected, GoogleApiClient.OnConnectionFailedListener {
    ArrayList<Ride> mList = new ArrayList<>();

    double radius = 10;
    LatLng mPlace;
    RidesAdapter mAdapter;
    GoogleApiClient mGoogleApiClient;
    RecyclerView recyclerView;
    ProgressDialogFragment mProgress;
    PreferencesHelper mHelper;
    ProgressDialogFragment dialog;
    Button mPostRide;
    PostInfo info;
    SwipeRefreshLayout mSwipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_available_rides);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        info = (PostInfo)intent.getSerializableExtra("post");

        mPlace = new LatLng(info.getStartLat(), info.getStarLon());
        mSwipe = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAvailableRides();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewRides);
        mProgress = new ProgressDialogFragment();
        mProgress.setTextMessage("Searching available Rides");
        mAdapter = new RidesAdapter(mList, this, Utils.TYPE_RIDER);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setCB1(new RidesAdapter.CB1() {
            @Override
            public void onClick(int action) {
                switch (action){
                    case Utils.POST_RIDE:
                        showSuccess();
                        break;
                    case Utils.PROFILE_INTERESTED_ONLY:
                        getAvailableRides();
                        break;
                }
            }
        });

        mHelper = new PreferencesHelper(this);
        mProgress.setTextMessage("Searching available Rides");
        mProgress.show(getSupportFragmentManager(), "F");
        mPostRide = (Button)findViewById(R.id.post_ride);

        mPostRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSuccess();
            }
        });

        if(recyclerView != null) {
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            getAvailableRides();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_post, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            Intent intent = new Intent();
            intent.putExtra("data", Utils.PROFILE_MY_POSTS);
            setResult(Utils.PROFILE_MY_POSTS, intent);
            finish();
            return true;
        }else if(id == R.id.post){
            showSuccess();
        }
        return super.onOptionsItemSelected(item);
    }

    void getAvailableRides(){
        HTTP.makeHttpGetRequest(this, Utils.GET_REQUESTED_RIDES+"/"+mHelper.getUserId(), this);
    }

    @Override
    public void onRequestComplete(String response) {
        JSONArray mResArray;
        mProgress.dismiss();
        mList.clear();
        try{
            mResArray = new JSONArray(response);
            mList.clear();
            for(int i=0; i<mResArray.length(); i++){
                JSONObject object = mResArray.getJSONObject(i);
                Ride ride = new Ride();
                ride.setPostId(object.getString("Riderid"));
                ride.setDriverId(object.getString("Userid"));
                ride.setStart(object.getString("StartFrom"));
                ride.setDestination(object.getString("Destination"));
                ride.setStartLoc(new LatLng(Double.valueOf(object.getString("LatStartFrom")), Double.valueOf(object.getString("LongStartFrom"))));
                ride.setDestinationLoc(new LatLng(Double.valueOf(object.getString("LatDestination")), Double.valueOf(object.getString("LongDestination"))));
                ride.setLeavingTime(object.getString("LeavingTime"));
                ride.setInterestedPeople(object.getString("Count"));
                ride.setPicture(object.getString("ProfilePic"));
                ride.setPhone(object.getString("Phone"));
                ride.setEmail(object.getString("Email"));
                ride.setName(object.getString("Name"));
                if(isInsideRadius(ride.getDestinationLoc())){
                    Log.v("Location", "Location Added");
                    mList.add(ride);
                }else{
                    Log.v("Location", "Location Cannot be Added");
                }

            }
            Log.d("AvailableRides", "Total available Rides : "+mList.size());
            if(mList.size() == 0){
                findViewById(R.id.empty).setVisibility(View.VISIBLE);
            }else{
                findViewById(R.id.empty).setVisibility(View.INVISIBLE);
            }
            mAdapter.notifyDataSetChanged();
            if(mSwipe.isRefreshing()){
                mSwipe.setRefreshing(false);
            }
            Log.v("Available Rides", response);

        }catch (JSONException ex){
            Log.e("JSONError", ex.getMessage());
        }
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, "Error while getting data from server", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationSelected(double radius, Place place) {
        this.radius = radius;
        Places.GeoDataApi.getPlaceById(mGoogleApiClient, place.getId())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {

                        if (places.getStatus().isSuccess()) {
                            final Place myPlace = places.get(0);
                            LatLng loc = myPlace.getLatLng();
                            mPlace = loc;
                            getAvailableRides();
                            mProgress.show(getSupportFragmentManager(),"Dialog");
                        }
                        places.release();
                    }
                });

    }

    boolean isInsideRadius(LatLng end){

        Location location1 = new Location("Location1");
        location1.setLatitude(end.latitude);
        location1.setLongitude(end.longitude);

        Location location2 = new Location("Location2");
        location2.setLatitude(mPlace.latitude);
        location2.setLongitude(mPlace.longitude);

        double e = location1.distanceTo(location2)/1000;

        Log.v("DISTANCE", "Distance : "+e+" To"+ mPlace.latitude+", "+mPlace.longitude+" From : "+end.latitude+", "+end.longitude);

        if(e>25){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void showSuccess(){
        FragmentManager fm = getSupportFragmentManager();
        DriverPostExtraInfo dialog = new DriverPostExtraInfo();
        Bundle bundle = new Bundle();
        bundle.putSerializable("post", info);
        dialog.setArguments(bundle);
        dialog.show(fm, "D");
    }

}
