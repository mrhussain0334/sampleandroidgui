package com.novelsol.hamrahi.Services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.ui.HamrahiApplication;

public class LocationService extends Service implements LocationListener {
    private static final String COMMAND = "command";
    private static final int START = 111;
    private static final int STOP = 222;
    private final BroadcastReceiver googleApiClientReadyReciever = new C05331();
    private int lastCommand;
    public static Location mLocation;
    private static boolean isLocationUpdated = false;

    private static LocationRequestCompleteCallback mCallback;

    private static GoogleApiClient mGoogleApiClient;


    class C05331 extends BroadcastReceiver {
        C05331() {
        }

        public void onReceive(Context context, Intent intent) {
            if (LocationService.this.lastCommand != 111) {
                return;
            }
            if (ContextCompat.checkSelfPermission(LocationService.this.getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0 || ContextCompat.checkSelfPermission(LocationService.this.getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                Location location = LocationServices.FusedLocationApi.getLastLocation(HamrahiApplication.getLocationTestApplication().getmGoogleApiClient());
                isLocationUpdated = true;
                if (location != null) {
                    LocationService.this.onLocationChanged(location);
                }
                Utils.mCallback.onLocationRequestCompleted();
                LocationService.this.startLocationCallbacks();
            }
        }
    }

    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = Utils.mGoogleApiClient;
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(this.googleApiClientReadyReciever, new IntentFilter(HamrahiApplication.API_CLIENT_CONNECTED));
        Log.d("LocationService","Service Created");
    }

    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(this.googleApiClientReadyReciever);
    }

    public static void startLocationCallbacks(Context context, GoogleApiClient mGoogleApiClient, LocationRequestCompleteCallback mCallback) {
        Utils.mGoogleApiClient = mGoogleApiClient;
        Utils.mCallback = mCallback;
        Log.d("LocationService","Hello");
        if(Utils.mGoogleApiClient==null){
            Log.d("LocationService","Google client null");
        }
        Intent intent = new Intent(context, LocationService.class);
        intent.putExtra(COMMAND, START);
        context.startService(intent);

    }

    public static void stopLocationCallbacks(Context context) {
        Intent intent = new Intent(context, LocationService.class);
        intent.putExtra(COMMAND, STOP);
        context.startService(intent);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            int command = intent.getIntExtra(COMMAND, STOP);
            this.lastCommand = command;
            switch (command) {
                case START:
                    startLocationCallbacks();
                    break;
                case STOP:
                    stopLocationCallbacks();
                    break;
            }
        }
        return 2;
    }

    private void stopLocationCallbacks() {
        if (!HamrahiApplication.getLocationTestApplication().isAPIClientConnected()) {
            return;
        }
        if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (LocationListener) this);
            stopSelf();
        }
    }

    private void startLocationCallbacks() {
        if (!HamrahiApplication.getLocationTestApplication().isAPIClientConnected()) {
            return;
        }
        if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (LocationListener) this);
            LocationRequest mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval((long) (60*1000));
            mLocationRequest.setFastestInterval(400);
            mLocationRequest.setSmallestDisplacement(12.0f);
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, mLocationRequest, (LocationListener) this);
        }
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onLocationChanged(Location location) {
        isLocationUpdated = true;
        mLocation = location;
        Utils.mCallback.onLocationRequestCompleted();
    }

    public static Location getLocation(){
        Log.d("LocationService","Location updated : "+isLocationUpdated);
        return mLocation;
    }

    public static boolean isLocationUpdated(){
        return isLocationUpdated;
    }

    public interface LocationRequestCompleteCallback{
        void onLocationRequestCompleted();
    }

}
