package com.novelsol.hamrahi.Services;

/**
 * Created by Shahzaib on 7/28/2017.
 */

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;

import java.util.HashMap;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    PreferencesHelper mHelper;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        mHelper = new PreferencesHelper(this);
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        Handler mainHandler = new Handler(Looper.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                sendRegistrationToServer(refreshedToken);
            }
        };
        mainHandler.post(myRunnable);
    }

    private void sendRegistrationToServer(String token) {
        HashMap<String, String> params = new HashMap<>();
        params.put("email", mHelper.getEmail());
        params.put("fcmid",token);

        HTTP.makeHttpPostRequest(this, Utils.POST_FCM, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                Log.d(TAG, "Token Posted Response : " + response);
            }

            @Override
            public void onError(String error) {
                Log.d(TAG, "Network error");
            }
        });

    }
}

