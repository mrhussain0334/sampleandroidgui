package com.novelsol.hamrahi.Services;

/**
 * Created by Shahzaib on 7/28/2017.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.novelsol.hamrahi.Database.DBHelper;
import com.novelsol.hamrahi.Helper.NotificationHelper;
import com.novelsol.hamrahi.MainActivity;
import com.novelsol.hamrahi.Model.Notification;
import com.novelsol.hamrahi.R;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]

    DBHelper dbHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        dbHelper = new DBHelper(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
       //     sendNotification(remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }
    }

    private void handleNotification(String str){
        JSONObject notification;
        try{
            notification = new JSONObject(str);
            String msg = notification.getString("msg");
            String title = notification.getString("title");
            int action = notification.getInt("action");
            showInfoNotification(title, msg, action);
        }catch (JSONException ex){
            Log.d("Notification", str);
            Log.e("Notification", ex.getMessage());
        }
    }

    private void showInfoNotification(String title, String body, int action) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("naction", action);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Log.d("NotiType", "Type : 1");


        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic)
                .setContentTitle(title)
                .setContentText(body.replace("\"", ""))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    private void handleAsDriver(JSONObject notification) throws JSONException{
        Notification obj = new Notification(notification.getString("notificationid"), notification.getString("name"), notification.getString("userid"), notification.getString("ownerid"), notification.getString("bookingid"), "Confirm rider", notification.getString("msg"), "r");
        Log.d("NotiType", "Type : 2");
        showInfoNotification(obj.getTitle(), obj.getMsg(), Integer.parseInt(obj.getId()));
        dbHelper.addNotification(obj);
    }
    private void handleAsRider(JSONObject notification) throws JSONException{
        Notification obj = new Notification(notification.getString("notificationid"), notification.getString("name"), notification.getString("userid"), notification.getString("ownerid"), notification.getString("bookingid"), "Confirm driver", notification.getString("msg"), "d");
        Log.d("NotiType", "Type : 3");
        showInfoNotification(obj.getTitle(), obj.getMsg(), Integer.parseInt(obj.getId()));
        dbHelper.addNotification(obj);
    }
}