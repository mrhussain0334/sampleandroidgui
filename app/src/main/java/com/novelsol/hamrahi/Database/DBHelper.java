package com.novelsol.hamrahi.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.novelsol.hamrahi.Model.Notification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ccn on 23/08/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "ridersharing";

    private static final int VERSION = 1;

    String KEY_NAME = "name";
    String KEY_USER_ID = "user_id";
    String KEY_TYPE = "type";
    String KEY_BOOKING_ID = "booking_id";
    String KEY_OWNER_ID = "owner_id";
    String TABLE_NOTIFICATIONS = "notifications";
    String ID = "id";

    String CREATE_TABLE_NOTIFICATIONS = "CREATE TABLE "+TABLE_NOTIFICATIONS+" ( "+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+KEY_NAME+" TEXT , "+KEY_USER_ID+" TEXT, "+KEY_TYPE+" TEXT, "+KEY_BOOKING_ID+" TEXT, "+KEY_OWNER_ID+" TEXT";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_NOTIFICATIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP IF EXITS "+TABLE_NOTIFICATIONS);
        onCreate(db);
    }


    public void addNotification(Notification notification){

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, notification.getName());
        values.put(KEY_USER_ID, notification.getTargetUserId());
        values.put(KEY_TYPE, notification.getType());
        values.put(KEY_BOOKING_ID, notification.getBookingId());
        values.put(KEY_OWNER_ID, notification.getSenderUserId());

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_NOTIFICATIONS, null, values);
        db.close();
    }


    public List<Notification> getNotifications(){
        List<Notification> notifications = new ArrayList<Notification>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATIONS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Notification notification = new Notification(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(5), cursor.getString(4), "","", cursor.getString(3));
                notifications.add(notification);
            } while (cursor.moveToNext());
        }

        return notifications;
    }

    public void removeNotification(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTIFICATIONS, ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }

    public List<Notification> getDriverNotifications(){
        List<Notification> notifications = new ArrayList<Notification>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATIONS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_NOTIFICATIONS, new String[] { ID,
                        KEY_NAME, KEY_USER_ID, KEY_TYPE, KEY_BOOKING_ID, KEY_OWNER_ID }, KEY_TYPE + "=?",
                new String[] { "d" }, null, null, null, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Notification notification = new Notification(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(5), cursor.getString(4), "","", cursor.getString(3));
                notifications.add(notification);
            } while (cursor.moveToNext());
        }

        return notifications;
    }

    public List<Notification> getRiderNotifications(){
        List<Notification> notifications = new ArrayList<Notification>();
        // Select All Query
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(TABLE_NOTIFICATIONS, new String[] { ID,
                        KEY_NAME, KEY_USER_ID, KEY_TYPE, KEY_BOOKING_ID, KEY_OWNER_ID }, KEY_TYPE + "=?",
                new String[] { "r" }, null, null, null, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Notification notification = new Notification(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(5), cursor.getString(4), "","", cursor.getString(3));
                notifications.add(notification);
            } while (cursor.moveToNext());
        }

        return notifications;
    }

}
