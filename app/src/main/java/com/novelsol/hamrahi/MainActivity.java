package com.novelsol.hamrahi;

import android.content.Context;
import android.content.DialogInterface;
import android.location.LocationManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kyleduo.switchbutton.SwitchButton;
import com.novelsol.hamrahi.Driver.DriverConfirmation;
import com.novelsol.hamrahi.Driver.DriverDashboard;
import com.novelsol.hamrahi.Fragments.ProfileFragment;
import com.novelsol.hamrahi.Fragments.Requests;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Rider.RiderDashboard;
import com.novelsol.hamrahi.Fragments.ScheduledContainer;
import com.novelsol.hamrahi.Fragments.VehicleFragment;
import com.novelsol.hamrahi.Helper.PreferencesHelper;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    PreferencesHelper mHelper;

    private static final int LOGIN_AS_DRIVER = 0;
    private static final int LOIGN_AS_RIDER = 1;


    Intent intent;

    View mNoLocationAvailable;
    DrawerLayout drawer;


    Button mOpenSettings;
    SwitchButton switchButton;
    public static ImageView mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        intent = getIntent();
        mHelper = new PreferencesHelper(this);

        if(!mHelper.isLogin()){
            startActivity(new Intent(this, SelectionActivity.class));
            finish();
        }
        mOpenSettings = (Button)findViewById(R.id.open_settings);
        mOpenSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.inflateMenu(mHelper.getLoginAs()==LOGIN_AS_DRIVER?R.menu.menu_driver:R.menu.menu_rider);
        navigationView.setCheckedItem(R.id.nav_dashboard);
        View headerLayout = navigationView.getHeaderView(0);

        TextView textView = (TextView)headerLayout.findViewById(R.id.textView);
        textView.setText(mHelper.getLoginAs()==LOGIN_AS_DRIVER?"Logged in as Driver":"Logged in as Rider");
        mProfile = (ImageView)headerLayout.findViewById(R.id.imageView);
        Glide.with(this).load(mHelper.getUserImg()).fitCenter().centerCrop().error(R.drawable.ic_user_avatar).into(mProfile);
        switchButton = (SwitchButton)headerLayout.findViewById(R.id.iv);
        switchButton.setChecked(mHelper.getLoginAs()==LOGIN_AS_DRIVER);
        switchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchAccount();
            }
        });
        TextView textView1 = (TextView)headerLayout.findViewById(R.id.name);
        textView1.setText(mHelper.getUserName());
        mNoLocationAvailable = findViewById(R.id.turn_on_loc);

        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        try {
            boolean gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if(!gps_enabled){
                lock(true);
            }
        }catch (Exception ex){

        }
        if(intent.hasExtra("naction")){
            int action = intent.getIntExtra("naction", 0);

            Fragment newFragment = mHelper.getLoginAs()==LOGIN_AS_DRIVER?new DriverDashboard():new RiderDashboard();

            switch (action){
                case Utils.OPEN_REQUEST:{
                    newFragment = new Requests();
                    break;
                }case Utils.OPEN_CONFIRMATION:{
                    String type = mHelper.getLoginAs() == 0? Utils.TYPE_DRIVER:Utils.TYPE_RIDER;
                    newFragment = new DriverConfirmation();
                    Bundle bundle = new Bundle();
                    bundle.putString("type", type);
                    newFragment.setArguments(bundle);
                    break;
                }case Utils.OPEN_SCHEDULED_RIDES:{
                    newFragment = new ScheduledContainer();
                    Bundle bundle = new Bundle();
                    String type = mHelper.getLoginAs() == 0? Utils.TYPE_DRIVER:Utils.TYPE_RIDER;
                    if(type.equals("d")){
                        bundle.putString("type", "drives");
                    }else{
                        bundle.putString("type", "rides");
                    }
                    newFragment.setArguments(bundle);
                    break;
                }
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, newFragment);
            transaction.commit();

        }else{
            Fragment newFragment = mHelper.getLoginAs()==LOGIN_AS_DRIVER?new DriverDashboard():new RiderDashboard();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, newFragment);
            transaction.commit();
        }

        try{
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            sendRegistrationToServer(refreshedToken);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        mHelper = new PreferencesHelper(this);
    }

    private void sendRegistrationToServer(String token) {
        HashMap<String, String> params = new HashMap<>();
        params.put("email", mHelper.getEmail());
        params.put("fcmid",token);

        HTTP.makeHttpPostRequest(this, Utils.POST_FCM, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                Log.d("MainActivity", "Token Posted Response : " + response);
            }

            @Override
            public void onError(String error) {
                Log.d("MainActivity", "Network error");
            }
        });

    }
    private void lock(boolean flag) {
        if(flag){
            mNoLocationAvailable.setVisibility(View.VISIBLE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }else{
            mNoLocationAvailable.setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }


    private void switchAccount() {
        new AlertDialog.Builder(this)
                .setTitle("Switch")
                .setMessage("You will be logged out you can re-login into desired mode")
                .setCancelable(false)
                .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        logout();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switchButton.setChecked(!switchButton.isChecked());
                    }
                })
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            boolean gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (gps_enabled) {
                lock(false);
            }
        } catch (Exception ex) {
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            exitApp();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        Fragment newFragment=  null;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (id == R.id.nav_dashboard) {
            newFragment = mHelper.getLoginAs()==LOGIN_AS_DRIVER?new DriverDashboard():new RiderDashboard();
        } else if (id == R.id.nav_profile) {
            newFragment = new ProfileFragment();
        } else if (id == R.id.nav_drives) {
            newFragment = new ScheduledContainer();
            Bundle bundle = new Bundle();
            bundle.putString("type", "drives");
            newFragment.setArguments(bundle);
        } else if (id == R.id.nav_rides) {
            newFragment = new ScheduledContainer();
            Bundle bundle = new Bundle();
            bundle.putString("type", "rides");
            newFragment.setArguments(bundle);
        } else if (id == R.id.nav_logout) {
            new AlertDialog.Builder(this)
                    .setTitle("Logout")
                    .setMessage("Are you sure you want to logout")
                    .setCancelable(false)
                    .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            logout();
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        } else if (id == R.id.nav_exit) {
            exitApp();
        }else if(id == R.id.nav_vehicles){
            newFragment = new VehicleFragment();
        }else if(id == R.id.nav_requests ){
            newFragment = new Requests();
        }else if(id == R.id.nav_confirm){
            String type = mHelper.getLoginAs() == 0? Utils.TYPE_DRIVER:Utils.TYPE_RIDER;
            newFragment = new DriverConfirmation();
            Bundle bundle = new Bundle();
            bundle.putString("type", type);
            newFragment.setArguments(bundle);
        }


        if(newFragment !=null){
            transaction.replace(R.id.container, newFragment);
            transaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void logout() {
        mHelper.setLogin(false);
        startActivity(new Intent(this,  LoginActivity.class));
        finish();
    }


    void exitApp(){
        new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }
}
