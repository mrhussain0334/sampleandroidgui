package com.novelsol.hamrahi.Model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Shahzaib on 7/30/2017.
 */

public class Drive {

    String postId;
    String driverId;
    String start;
    String destination;
    LatLng startLoc;
    LatLng destinationLoc;
    String leavingTime;
    String interestedPeople;

    public Drive(String postId, String driverId, String start, String destination, LatLng startLoc, LatLng destinationLoc, String leavingTime, String interestedPeople) {
        this.postId = postId;
        this.driverId = driverId;
        this.start = start;
        this.destination = destination;
        this.startLoc = startLoc;
        this.destinationLoc = destinationLoc;
        this.leavingTime = leavingTime;
        this.interestedPeople = interestedPeople;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public LatLng getStartLoc() {
        return startLoc;
    }

    public void setStartLoc(LatLng startLoc) {
        this.startLoc = startLoc;
    }

    public LatLng getDestinationLoc() {
        return destinationLoc;
    }

    public void setDestinationLoc(LatLng destinationLoc) {
        this.destinationLoc = destinationLoc;
    }

    public String getLeavingTime() {
        return leavingTime;
    }

    public void setLeavingTime(String leavingTime) {
        this.leavingTime = leavingTime;
    }

    public String getInterestedPeople() {
        return interestedPeople;
    }

    public void setInterestedPeople(String interestedPeople) {
        this.interestedPeople = interestedPeople;
    }
}
