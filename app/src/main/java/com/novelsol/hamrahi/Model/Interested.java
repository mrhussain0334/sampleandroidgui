package com.novelsol.hamrahi.Model;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by ccn on 27/07/2017.
 */

public class Interested implements Serializable {
    String name;
    String phone;
    String picture;
    String id;
    String status;
    String bookingId;
    String to;
    String from;
    String time;
    String type;

    public Interested(String id,String name, String phone, String picture, String status) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.picture = picture;
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
