package com.novelsol.hamrahi.Model;

import java.io.Serializable;

/**
 * Created by ccn on 27/07/2017.
 */

public class Scheduled implements Serializable{
    String bookingId;
    String userId;
    String time;
    String interested;
    String picture;
    String to;
    String from;
    int section;
    String type;
    String riderId;
    String dirverId;
    String vehicleName;
    String vehicleNumber;
    String vehicleType;

    int status;


    public Scheduled(String bookingId, String userId, String time, String interested, String picture) {
        this.bookingId = bookingId;
        this.userId = userId;
        this.time = time;
        this.interested = interested;
    }

    public Scheduled(String bookingId, String userId, String time, String picture) {
        this.bookingId = bookingId;
        this.userId = userId;
        this.time = time;
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getInterested() {
        return interested;
    }

    public void setInterested(String interested) {
        this.interested = interested;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
}
