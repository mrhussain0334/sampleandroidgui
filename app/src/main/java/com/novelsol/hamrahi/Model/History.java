package com.novelsol.hamrahi.Model;

/**
 * Created by Shahzaib on 7/30/2017.
 */

public class History {
    String from;
    String to;
    String time;
    String pic;
    String name;
    String rating;

    public History(String from, String to, String time, String pic, String name) {
        this.from = from;
        this.to = to;
        this.time = time;
        this.pic = pic;
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDriverPic() {
        return pic;
    }

    public void setDriverPic(String driverPic) {
        this.pic = driverPic;
    }

    public String getDriverName() {
        return name;
    }

    public void setDriverName(String driverName) {
        this.name = driverName;
    }
}
