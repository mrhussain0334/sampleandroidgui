package com.novelsol.hamrahi.Model;

import com.google.android.gms.maps.model.LatLng;
import com.novelsol.hamrahi.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hhala on 5/29/2017.
 */
public class Ride {
    String postId;
    String driverId;
    String start;
    String destination;
    LatLng startLoc;
    LatLng destinationLoc;
    String leavingTime;
    String interestedPeople;
    String picture;
    String name;
    String email;
    String phone;
    String vehicleName;
    String vehicleNumber;
    String seats;

    public Ride(){

    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public LatLng getStartLoc() {
        return startLoc;
    }

    public void setStartLoc(LatLng startLoc) {
        this.startLoc = startLoc;
    }

    public LatLng getDestinationLoc() {
        return destinationLoc;
    }

    public void setDestinationLoc(LatLng destinationLoc) {
        this.destinationLoc = destinationLoc;
    }

    public String getLeavingTime() {
        return leavingTime;
    }

    public void setLeavingTime(String leavingTime) {
        this.leavingTime = leavingTime;
    }

    public String getInterestedPeople() {
        return interestedPeople;
    }

    public void setInterestedPeople(String interestedPeople) {
        this.interestedPeople = interestedPeople;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }
}
