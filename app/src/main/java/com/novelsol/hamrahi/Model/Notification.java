package com.novelsol.hamrahi.Model;

import java.io.Serializable;

/**
 * Created by Shahzaib on 7/30/2017.
 */

public class Notification implements Serializable{
    String id;
    String name;
    String targetUserId;
    String senderUserId;
    String bookingId;
    String title;
    String msg;
    String type;

    public Notification(String id, String name, String targetUserId, String senderUserId, String bookingId, String title, String msg, String type) {
        this.id = id;
        this.name = name;
        this.targetUserId = targetUserId;
        this.senderUserId = senderUserId;
        this.bookingId = bookingId;
        this.title = title;
        this.msg = msg;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(String targetUserId) {
        this.targetUserId = targetUserId;
    }

    public String getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(String senderUserId) {
        this.senderUserId = senderUserId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

//    public Notification from(Interested interested){
//        new Notification();
//    }
}
