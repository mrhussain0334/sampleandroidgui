package com.novelsol.hamrahi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.ui.MyEditText;
import com.novelsol.hamrahi.ui.MyTextView;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;
import com.tapadoo.alerter.Alerter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{


    MyEditText mEmail, mPassword;
    MyTextView mLogin, mForgot, mSignup;
    PreferencesHelper mHelper;
    boolean f1, f2;
    ProgressDialogFragment mProgress;
    ToggleSwitch toggleSwitch;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEmail = (MyEditText)findViewById(R.id.et_email);
        mPassword = (MyEditText)findViewById(R.id.et_password);
        mLogin = (MyTextView)findViewById(R.id.btn_signin);
        mForgot = (MyTextView)findViewById(R.id.forgot);
        mSignup = (MyTextView)findViewById(R.id.signup);
        mProgress = new ProgressDialogFragment();
        mProgress.setTextMessage("Logging in..");
        mLogin.setOnClickListener(this);
        mHelper = new PreferencesHelper(this);
        mSignup.setOnClickListener(this);
        mForgot.setOnClickListener(this);
        toggleSwitch = (ToggleSwitch) findViewById(R.id.switch_toggle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_signin:{
                if(isValidEmail(mEmail.getText().toString())){
                    f1 = true;
                }else{
                    mEmail.setError("Email is not valid");
                    f1 = false;
                }

                if(mPassword.getText().length()!=0){
                    f2 = true;
                }else{
                    mPassword.setError("Password can not be  empty");
                    f2 = false;
                }

                if(f1&&f2){
                    login();
                }
                break;
            }case R.id.signup:{
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
                break;
            }case R.id.forgot:{

                break;
            }
        }
    }

    void login(){
        HashMap<String, String> params = new HashMap<>();
        params.put("email", mEmail.getText().toString());
        params.put("password", mPassword.getText().toString());

        mProgress.show(getSupportFragmentManager(),"Dialog");

        HTTP.makeHttpPostRequest(this, Utils.LOGIN, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                JSONObject res;
                mProgress.dismiss();
                try{
                    res = new JSONObject(response);

                    mHelper.setUserId(res.getString("Userid"));
                    mHelper.setUserName(res.getString("Name"));
                    mHelper.setUserPhone(res.getString("Phone"));
                    mHelper.setEmail(res.getString("Email"));
                    mHelper.setPassword(res.getString("Password"));
                    mHelper.setUserImg(res.getString("ProfilePic"));
                    mHelper.setLogin(true);

                    String verified = res.getString("VerifyStatus");

                    if(verified.equalsIgnoreCase("N")){
                        mHelper.setLogin(false);
                        Intent intent = new Intent(LoginActivity.this, MobileVerificationActivity.class);
                        intent.putExtra("phone", res.getString("Phone"));
                        startActivity(intent);
                        finish();
                    }else{
                        mHelper.setLogin(true);
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("loginas",toggleSwitch.getCheckedTogglePosition());
                        mHelper.setLoginAs(toggleSwitch.getCheckedTogglePosition());
                        startActivity(intent);
                        finish();
                    }

                }catch (JSONException ex){
                    ex.printStackTrace();
                    Toast.makeText(LoginActivity.this, response, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                mProgress.dismiss();
                Toast.makeText(LoginActivity.this, "Networking error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public boolean isValidEmail(String email){
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public boolean isValidPassword(String password){
        String PHONE_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        Matcher matcher = pattern.matcher(password);

        return matcher.matches();
    }

}
