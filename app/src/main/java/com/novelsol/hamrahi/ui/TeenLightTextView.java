package com.novelsol.hamrahi.ui;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.novelsol.hamrahi.Helper.TypefaceProvider;

/**
 * Created by Shahzaib on 5/28/2017.
 */

public class TeenLightTextView extends AppCompatTextView {
    public TeenLightTextView(Context context) {
        super(context);
        setTypeface(TypefaceProvider.getLightTypeface(context));
    }

    public TeenLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(TypefaceProvider.getLightTypeface(context));
    }

    public TeenLightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(TypefaceProvider.getLightTypeface(context));
    }
}
