package com.novelsol.hamrahi.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import java.util.HashMap;

/**
 * Created by Shahzaib on 5/28/2017.
 */

public class CustomAutoCompleteEditText extends android.support.v7.widget.AppCompatAutoCompleteTextView {
    public CustomAutoCompleteEditText(Context context) {
        super(context);
    }

    public CustomAutoCompleteEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomAutoCompleteEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        /** Each item in the autocompetetextview suggestion list is a hashmap object */
        HashMap<String, String> hm = (HashMap<String, String>) selectedItem;
        return hm.get("description");
    }
}
