package com.novelsol.hamrahi.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.novelsol.hamrahi.R;

/**
 * Created by ccn on 26/07/2017.
 */

public class ProgressDialogFragment extends DialogFragment {

    String text;

    public ProgressDialogFragment(){
        setCancelable(false);
    }

    public void setTextMessage(String str){
        this.text = str;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.loading, container, false);
        MyTextView mLoadingText = (MyTextView)view.findViewById(R.id.loadingtext);
        mLoadingText.setText(text);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }
}
