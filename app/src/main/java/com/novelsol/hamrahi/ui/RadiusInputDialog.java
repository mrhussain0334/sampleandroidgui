package com.novelsol.hamrahi.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.novelsol.hamrahi.R;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by Shahzaib on 7/31/2017.
 */

public class RadiusInputDialog extends DialogFragment {
    public RadiusInputDialog(){
        setCancelable(false);
    }
    OnLocationSelected mCallback;
    Place mPlace;
    int REQUEST_CODE_AUTOCOMPLETE = 45;
    EditText mEditText;
    private static final LatLngBounds BOUNDS = new LatLngBounds(
            new LatLng(31.401422, 74.211078), new LatLng(31.554606, 74.357158));

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.radius_input, container, false);
    mEditText = (EditText)view.findViewById(R.id.et);


        Button searchLocations = (Button)view.findViewById(R.id.search_rides);
        searchLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPlace  == null){
                    Toast.makeText(getActivity(), "Please select location first", Toast.LENGTH_SHORT);
                }else{
                    mCallback.onLocationSelected(5, mPlace);
                    dismiss();
                }
            }
        });

        View search = view.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAutocompleteActivity();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    public void setOnLocationSelectedListener(OnLocationSelected mCallback){
        this.mCallback = mCallback;
    }

    private void openAutocompleteActivity() {

        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setBoundsBias(BOUNDS)
                    .build(getActivity());
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);

        } catch (GooglePlayServicesRepairableException e) {
            GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), e.getConnectionStatusCode(),

                    0 /* requestCode */).show();

        } catch (GooglePlayServicesNotAvailableException e) {
            String message = "Google Play Services is not available: " +

                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);


            Log.e("SelectPlace", message);
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

        }

    }

    public interface OnLocationSelected{
        void onLocationSelected(double radius, Place place);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {

            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                mEditText.setText(place.getAddress().toString());
                Log.i("SelectLocation", "Place Selected: " + place.getName());
                mPlace = place;
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Log.e("SelectLocation", "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    getActivity().onBackPressed();
                    return true;
                }
                return false;
            }
        });
    }

}
