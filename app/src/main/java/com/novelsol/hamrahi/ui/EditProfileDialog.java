package com.novelsol.hamrahi.ui;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

/**
 * Created by ccn on 26/07/2017.
 */
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ccn on 05/06/2017.
 */

public class EditProfileDialog extends DialogFragment implements View.OnClickListener {


    public static final int EDIT_NAME = 1;
    public static final int EDIT_PASSWORD = 2;

    static int type;

    MyEditText mInput;
    MyEditText mOldPassword;

    OnDataSavedCallback callback;

    PreferencesHelper mHelper;
    String oldPassword;
    String newPassword;
    ProgressDialogFragment mProgress;

    public EditProfileDialog() {

    }

    public static EditProfileDialog getInstance(int type){
        EditProfileDialog.type = type;
        return new EditProfileDialog();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        mHelper = new PreferencesHelper(getContext());
        Bundle bundle = getArguments();

        mHelper = new PreferencesHelper(getContext());

        switch (type){
            case EDIT_PASSWORD:{
                view =  inflater.inflate(R.layout.d_edit_password, container);
                mOldPassword = (MyEditText)view.findViewById(R.id.old_password);
                mInput = (MyEditText)view.findViewById(R.id.new_password);

                break;
            }default:{
                view =  inflater.inflate(R.layout.d_edit_profile, container);
                MyTextView mTitle = (MyTextView) view.findViewById(R.id.title);
                mTitle.setText(bundle.getString("title"));
                mInput = (MyEditText)view.findViewById(R.id.input);
                mInput.setHint("Enter name");
                Log.d("EDITPROFILE", "Type : "+type);
            }
            Log.d("EDITPROFILE", "Type : "+type);
        }

        Button mSave = (Button)view.findViewById(R.id.save);
        mSave.setOnClickListener(this);
        Button mCancel = (Button)view.findViewById(R.id.cancel);
        mCancel.setOnClickListener(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.save:{
                switch (type){
                    case EDIT_NAME:{
                        mHelper.setUserName(mInput.getText().toString());
                        callback.onSave(EDIT_NAME, mInput.getText().toString());
                        dismiss();
                        break;
                    }case EDIT_PASSWORD:{
                        oldPassword = mOldPassword.getText().toString();
                        newPassword = mInput.getText().toString();

                        if(oldPassword.length()==0){
                            mOldPassword.setError("Field cannot be empty");
                            break;
                        }if(newPassword.length()==0){
                            mInput.setError("Field cannot be empty");
                            break;
                        }
                        callNetwork(EDIT_PASSWORD);
                    }
                }
                break;
            }case R.id.cancel:{
                dismiss();
                break;
            }
        }
    }


    private void callNetwork(int editPassword) {
        String url="";
        HashMap<String, String> params = new HashMap<>();
        switch (editPassword){
            case EDIT_PASSWORD:{
                url = Utils.CHANGE_PASSWORD;
                params.put("id", mHelper.getEmail());
                params.put("oldpassword", oldPassword);
                params.put("newpassword", newPassword);
                break;
            }case EDIT_NAME:{
                break;
            }
        }




        HTTP.makeHttpPostRequest(getContext(), url, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("Success")){
                    dismiss();
                }else{
                    Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    public boolean isValidPassword(String password){
        String PHONE_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=*!])(?=\\S+$).{8,}$";
        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        Matcher matcher = pattern.matcher(password);

        return matcher.matches();
    }

    public void setOnDataSavedCallback(OnDataSavedCallback callback){
        this.callback = callback;
    }

    public interface OnDataSavedCallback{
        void onSave(int type, String newData);
    }
}