package com.novelsol.hamrahi.ui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.novelsol.hamrahi.Helper.TypefaceProvider;

/**
 * Created by Shahzaib on 5/28/2017.
 */

public class TeenRegularTextView extends android.support.v7.widget.AppCompatTextView {
    public TeenRegularTextView(Context context) {
        super(context);
        setTypeface(TypefaceProvider.getRegularTypeface(context));

    }

    public TeenRegularTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setTypeface(TypefaceProvider.getRegularTypeface(context));
    }

    public TeenRegularTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(TypefaceProvider.getRegularTypeface(context));
    }
}
