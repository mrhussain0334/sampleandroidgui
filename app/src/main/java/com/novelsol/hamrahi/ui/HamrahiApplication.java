package com.novelsol.hamrahi.ui;

/**
 * Created by ccn on 16/08/2017.
 */

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;

public class HamrahiApplication extends Application implements ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final String API_CLIENT_CONNECTED = "API_CONNECTED";
    static HamrahiApplication instance;
    private boolean isConnected;
    private GoogleApiClient mGoogleApiClient;

    public static HamrahiApplication getLocationTestApplication() {
        return instance;
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public void onCreate() {
        super.onCreate();
        instance = this;

        Log.d("HamrahiApplication","HamrahiApplication Created");

//        this.mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(getApplicationContext(), 0, this)
//                .addApi(LocationServices.API)
//                .build();
//        if(mGoogleApiClient!=null){
//            this.mGoogleApiClient.connect();
//        }else{
//            Log.d("HamrahiApplication","Null object");
//        }

//        Log.v("HamrahiApplication", "Connected : "+mGoogleApiClient.isConnected());
    }

    public boolean isAPIClientConnected() {
        return this.isConnected;
    }

    public GoogleApiClient getmGoogleApiClient() {
        return this.mGoogleApiClient;
    }



    public void onConnected(@Nullable Bundle bundle) {
        this.isConnected = true;
        Log.d("HamrahiApplication","HamrahiApplication Connected");
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(API_CLIENT_CONNECTED));
    }

    public void onConnectionSuspended(int i) {
        this.isConnected = false;
        Log.d("HamrahiApplication","HamrahiApplication Connection suspended");
        this.mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("HamrahiApplication", "Connection failed : "+connectionResult.getErrorMessage());
    }
}