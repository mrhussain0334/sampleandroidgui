package com.novelsol.hamrahi.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.PostInfo;
import com.novelsol.hamrahi.Model.Vehicle;
import com.novelsol.hamrahi.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ccn on 07/11/2017.
 */

public class DriverPostExtraInfo extends DialogFragment {
    Spinner mVehicle;
    ArrayList<String> mVehicles = new ArrayList<>();
    private static final String ROOT = "http://ufmo.asia/";
    private static final String KEY = "qweasdzxc123";
    private static final String GET = ROOT + "RidesharingApi/public/vehicle/get/" + KEY;
    private static AsyncHttpClient client = new AsyncHttpClient();
    PreferencesHelper mHelper;
    ArrayList<Vehicle> mAllVehicles = new ArrayList<>();
    Bundle bundle;
    TextView mPost, mCancel;
    ProgressDialogFragment dialog;
    LinearLayout ll;
    TextView mAddText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_diver_post_extra_info, container, false);
        mVehicle = (Spinner)view.findViewById(R.id.vehicle);
        mVehicles.add("Choose vehicle");
        dialog = new ProgressDialogFragment();
        mPost = (TextView)view.findViewById(R.id.post);
        ll = (LinearLayout)view.findViewById(R.id.ll);
        mCancel = (TextView) view.findViewById(R.id.cancel);
        mPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPost.getText().toString().equalsIgnoreCase("add")){
                    Intent intent = new Intent();
                    intent.putExtra("data", Utils.ADD_VEHICLE);
                    getActivity().setResult(Utils.ADD_VEHICLE, intent);
                    dismiss();
                    getActivity().finish();
                }else{
                    if(mVehicle.getSelectedItemPosition() != 0){
                        post();
                    }else{
                        Toast.makeText(getContext(), "Please choose a vehicle", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        mAddText = (TextView)view.findViewById(R.id.add_txt);
        dialog = new ProgressDialogFragment();
        dialog.show(getActivity().getSupportFragmentManager(), "P");
        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        bundle = getArguments();
        mHelper  = new PreferencesHelper(getContext());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mVehicles);
        mVehicle.setAdapter(adapter);
        mVehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==mVehicles.size()-1){

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        getVehicles(mHelper.getUserId(), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    dialog.dismiss();
                    String response= new String(responseBody);
                    JSONArray newResponse = new JSONArray(response);
                    for (int i = 0; i < newResponse.length();i++) {
                        JSONObject object = (JSONObject) newResponse.get(i);
                        Vehicle v = new Vehicle();
                        v.setVehicleId(object.getInt("Vehicleid"));
                        v.setCapacity(object.getInt("Capacity"));
                        v.setColor(object.getString("Color"));
                        v.setMake(object.getString("Make"));
                        v.setModel(object.getString("Model"));
                        v.setUserId(object.getInt("Userid"));
                        v.setRegNo(object.getString("RegNo"));
                        v.setVehicleType(object.getString("VehicleType"));
                        mAllVehicles.add(v);
                        mVehicles.add(object.getString("Model")+" - "+object.getString("RegNo"));
                    }
                    if(mAllVehicles.size() == 0){
                        mPost.setText("ADD");
                        mAddText.setVisibility(View.VISIBLE);
                        ll.setVisibility(View.GONE);
                    }
                    mVehicle.getAdapter().notifyAll();
                }
                catch (Exception e) {
                    Log.e("Error",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
        return view;
    }
    public static void getVehicles(String id, AsyncHttpResponseHandler handler)
    {
        try {
            client.get(GET + "/" + id,null,handler);
        }
        catch (Exception e)
        {
            Log.e("Error",e.getMessage());
        }
    }

    private void post(){

        int id = 0;
        for(int i =0; i<mAllVehicles.size();i++){
            Vehicle vehicle = mAllVehicles.get(i);
            if(vehicle.getModel().equals(mVehicle.getSelectedItem().toString().split(" - ")[0])){
                id = vehicle.getVehicleId();
            }
        }

        PostInfo info = (PostInfo) bundle.getSerializable("post");
        HashMap<String, String> params = new HashMap<>();

        params.put("userid", mHelper.getUserId());
        params.put("startfrom", info.getStartAddress());
        params.put("destination", ""+info.getEndAddress());

        Log.v("LATLONG", "lon = "+String.valueOf(info.getEndLon())+" , lat = "+ String.valueOf(info.getEndLat()));

        params.put("leavingtime", info.getLeavingTime());
        params.put("longstartfrom", String.valueOf(info.getStarLon()));
        params.put("latstartfrom", String.valueOf(info.getStartLat()));
        params.put("longdestination", String.valueOf(info.getEndLon()));
        params.put("latdestination", String.valueOf(info.getEndLat()));
        params.put("vehicleid", String.valueOf(id));

        Log.v("Pick", "Leaving time : "+ info.getLeavingTime());

        HTTP.makeHttpPostRequest(getContext(), Utils.POST_DRIVER_REQUEST, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    dialog .dismiss();
                    Intent intent = new Intent();
                    intent.putExtra("data", Utils.PROFILE_MY_POSTS);
                    getActivity().setResult(Utils.PROFILE_MY_POSTS, intent);
                    dismiss();
                    getActivity().finish();
                }else{
                    dialog.dismiss();
                    Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError(String error) {

            }
        });
        dialog = new ProgressDialogFragment();
        dialog.setTextMessage("Please wait while we are posting your request.");
        dialog.show(((FragmentActivity)getContext()).getSupportFragmentManager(), "P");
    }
}
