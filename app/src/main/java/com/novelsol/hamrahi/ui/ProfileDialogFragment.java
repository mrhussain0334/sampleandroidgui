package com.novelsol.hamrahi.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.MainActivity;
import com.novelsol.hamrahi.R;

import org.w3c.dom.Text;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shahzaib on 7/31/2017.
 */

public class ProfileDialogFragment extends DialogFragment implements View.OnClickListener{

    public ProfileDialogFragment(){

    }

    TextView mName;
    TextView mPhone;
    View mActionCall;
    View mActionMsg;
    View mActionLoc;
    CircleImageView mImage;
    TextView mYesBtn;
    TextView mNoBtn;

    AlertDialog.Builder builder;
    ClickCallback mCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_info_dialog, container, false);
        setCancelable(false);
        return view;
    }

    public void setCallBack(ClickCallback mCallback){
        this.mCallback = mCallback;
    }

    Bundle bundle;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mName = (TextView)view.findViewById(R.id.name);
        mPhone = (TextView)view.findViewById(R.id.phone);
        mActionCall = view.findViewById(R.id.action_call);
        mActionMsg = view.findViewById(R.id.action_msg);
        mActionLoc = view.findViewById(R.id.action_loc);
        mYesBtn = (TextView)view.findViewById(R.id.yesBtn);
        mNoBtn = (TextView)view.findViewById(R.id.noBtn);
        mActionLoc.setOnClickListener(this);
        mActionMsg.setOnClickListener(this);
        mActionCall.setOnClickListener(this);
        mName.setText(bundle.getString("name"));
        mPhone.setText(bundle.getString("phone"));
        mImage = (CircleImageView)view.findViewById(R.id.pic);
        Glide.with(getContext()).load(bundle.getString("pic")).error(R.drawable.ic_user_avatar).centerCrop().fitCenter().into(mImage);
        mYesBtn.setOnClickListener(this);
        mNoBtn.setOnClickListener(this);
        builder = new AlertDialog.Builder(getActivity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.action_call:{
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" +bundle.getString("phone")));
                startActivity(intent);
                break;
            }case R.id.action_msg:{
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", mPhone.getText().toString());
                smsIntent.putExtra("sms_body","Hi, "+mName.getText().toString());
                startActivity(smsIntent);
                break;
            }case R.id.action_loc:{
                String uri = "http://maps.google.com/maps?q=loc:" + bundle.getDouble("lat") + "," + bundle.getDouble("long") + " (Start Location)";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                getContext().startActivity(intent);
                break;
            }case R.id.noBtn:{
                builder.setMessage("You want to post this rider?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        mCallback.onClick(Utils.POST_RIDE);
                        dismiss();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        mCallback.onClick(Utils.PROFILE_MSG_SENT_REQUESTS);
                        dismiss();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                break;
            }case R.id.yesBtn:{
                mCallback.onClick(Utils.PROFILE_INTERESTED_ONLY);
                dismiss();
            }

        }
    }

    public interface ClickCallback{
        void onClick(int mode);
    }
}