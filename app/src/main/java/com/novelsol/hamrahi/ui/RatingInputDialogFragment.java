package com.novelsol.hamrahi.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.R;

import java.util.HashMap;

/**
 * Created by shahz on 10/15/2017.
 */

public class RatingInputDialogFragment extends DialogFragment implements View.OnClickListener {

    RatingBar mRating;
    EditText mReview;
    View pClick1, pClick2;
    Bundle bundle;
    ReviewPostCallback mCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.rate_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRating = (RatingBar)view.findViewById(R.id.ratingBar2);
        mRating.setRating(2);
        bundle = getArguments();
        mReview = (EditText)view.findViewById(R.id.review2);
        pClick1 = view.findViewById(R.id.p_click1);
        pClick2 = view.findViewById(R.id.p_click2);
        pClick1.setOnClickListener(this);
        pClick2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        HashMap<String, String> params = new HashMap<>();
        params.put("postid", bundle.getString("postid"));
        params.put("type",bundle.getString("type").equalsIgnoreCase("r")?"d":"r");
        params.put("userid", bundle.getString("userid"));
        params.put("rating", mRating.getRating()+"");
        params.put("review", mReview.getText().toString());
        HTTP.makeHttpPostRequest(getContext(), Utils.POST_REVIEW, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    Toast.makeText(getContext(), "Review posted", Toast.LENGTH_SHORT).show();
                    dismiss();
                    mCallback.onReviewPosted();
                }else{
                    Log.v("Error", response);
                    Toast.makeText(getContext(), "Error occurred", Toast.LENGTH_SHORT).show();
                    dismiss();
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void refresh(){

    }

    public void setReviewPostCallback(ReviewPostCallback mCallback){
        this.mCallback = mCallback;
    }

    public interface ReviewPostCallback{
        void onReviewPosted();
    }
}
