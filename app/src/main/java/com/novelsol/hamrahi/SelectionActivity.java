package com.novelsol.hamrahi;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class SelectionActivity extends AppCompatActivity {

    TextView signin;
    TextView signup;
    HashMap<String, Boolean> permissions;
    ArrayList<String> mPermissionsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
        signin = (TextView)findViewById(R.id.sign_in);
        signup = (TextView)findViewById(R.id.register);
        permissions = new HashMap<>();
        mPermissionsList = new ArrayList<>();
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(SelectionActivity.this,LoginActivity.class);
                startActivity(it);
                finish();
            }
        });


        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(SelectionActivity.this,RegisterActivity.class);
                startActivity(it);
                finish();

            }
        });
        checkPermissions();
    }


    void checkPermissions(){

        boolean sendSmsPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED;
        boolean receiveSmsPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED;
        boolean callPhonePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED;
        boolean networkPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED;
        boolean readCoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean readFineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean accessCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        boolean accessStorage1 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        boolean accessStorage2 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

        permissions.put(Manifest.permission.SEND_SMS, sendSmsPermission);
        permissions.put(Manifest.permission.RECEIVE_MMS, receiveSmsPermission);
        permissions.put(Manifest.permission.CALL_PHONE, callPhonePermission);
        permissions.put(Manifest.permission.ACCESS_NETWORK_STATE, networkPermission);
        permissions.put(Manifest.permission.ACCESS_COARSE_LOCATION, readCoarseLocation);
        permissions.put(Manifest.permission.ACCESS_FINE_LOCATION, readFineLocation);
        permissions.put(Manifest.permission.CAMERA,accessCamera);
        permissions.put(Manifest.permission.READ_EXTERNAL_STORAGE, accessStorage1);
        permissions.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, accessStorage2);

        int c = 0;
        Set<String> set = permissions.keySet();
        Iterator<String> it = set.iterator();
        while (it.hasNext()){
            String temp = it.next();
            if(!permissions.get(temp)){
                mPermissionsList.add(temp);
            }else {
                c++;
            }
        }
        if(c<9){
            ActivityCompat.requestPermissions(this, mPermissionsList.toArray(new String[mPermissionsList.size()]), 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        for(int grant: grantResults){
            if(grant == PackageManager.PERMISSION_DENIED){
                new AlertDialog.Builder(SelectionActivity.this)
                .setTitle("Alert")
                .setMessage("Without giving necessary permissions you won't be able to use the App")
                .setPositiveButton("Grant permissions", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       checkPermissions();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
                break;
            }
        }
    }
}