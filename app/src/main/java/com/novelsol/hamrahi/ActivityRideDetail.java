package com.novelsol.hamrahi;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.novelsol.hamrahi.Adapters.InterestedPeopleAdapter;
import com.novelsol.hamrahi.Adapters.RequestsAdapter;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.*;
import com.novelsol.hamrahi.Model.Scheduled;
import com.novelsol.hamrahi.ui.MyTextView;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class ActivityRideDetail extends AppCompatActivity {

    RecyclerView mRecyclerView;
    TextView mFrom;
    TextView mTo;
    TextView mLeavingTime;
    Button mCancel;
    InterestedPeopleAdapter mAdapter;
    ArrayList<Interested> mList;
    String type;
    String bookingId;
    com.novelsol.hamrahi.Model.Scheduled obj;
    PreferencesHelper mHelper;

    ProgressDialogFragment mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_detail);
        mHelper = new PreferencesHelper(this);
        mList = new ArrayList<>();
        mProgress = new ProgressDialogFragment();
        mProgress.setTextMessage("Loading..");
        mProgress.show(getSupportFragmentManager(), "P");
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        mFrom = (TextView)findViewById(R.id.from);
        mTo = (TextView)findViewById(R.id.to);
        mLeavingTime = (TextView)findViewById(R.id.time);
        mCancel = (Button) findViewById(R.id.cancel);
        Intent intent = getIntent();
        obj = (Scheduled) intent.getSerializableExtra("obj");
        mFrom.setText(obj.getFrom());
        mTo.setText(obj.getTo());
        try {
            mLeavingTime.setText(new SimpleDateFormat("hh:mm").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(obj.getTime())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(ActivityRideDetail.this)
                        .setTitle("Cancel")
                        .setMessage("Do you really want to cancel this ride?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                cancel();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

        type = obj.getType();
        bookingId = obj.getBookingId();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        mAdapter = new InterestedPeopleAdapter(this, mList, bundle);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(mAdapter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Post details");
        callNetwork();
    }

    private void cancel() {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", obj.getBookingId());
        HTTP.makeHttpPostRequest(this, obj.getType().equalsIgnoreCase("r")?Utils.CANCEL_S_RIDER:Utils.CANCEL_S_DRIVER, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                JSONArray array;
                mProgress.dismiss();
                try{
                    Log.v("Interested", response);
                    array = new JSONArray(response);
                    for(int i=0; i<array.length(); i++){
                        JSONObject obj = array.getJSONObject(i);
                        Interested interested = new Interested(obj.getString("Userid"), obj.getString("Name"), obj.getString("Phone"), obj.getString("ProfilePic"), obj.getString("Status"));
                        interested.setBookingId(obj.getString("Bookingid"));
                        mList.add(interested);
                    }
                    Log.d("AvailableRides", "Total available Rides : "+mList.size());
                    if(mList.size() == 0){
                        findViewById(R.id.empty).setVisibility(View.VISIBLE);
                        ((MyTextView)findViewById(R.id.empty_txt)).setText("No requests");
                    }else{
                        mAdapter.notifyDataSetChanged();
                    }
                }catch (JSONException ex){
                    findViewById(R.id.empty).setVisibility(View.VISIBLE);
                    ((MyTextView)findViewById(R.id.empty_txt)).setText("No requests");
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(ActivityRideDetail.this, "Networking error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //finish();
                onBackPressed();
                break;
        }
        return true;
    }

    void callNetwork(){
        HashMap<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("postid", bookingId);
        HTTP.makeHttpPostRequest(this, Utils.INTERESTED_PEOPLE, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                JSONArray array;
                mProgress.dismiss();
                try{
                    Log.v("Interested", response);
                    array = new JSONArray(response);
                    for(int i=0; i<array.length(); i++){
                        JSONObject obj = array.getJSONObject(i);
                        Interested interested = new Interested(obj.getString("Userid"), obj.getString("Name"), obj.getString("Phone"), obj.getString("ProfilePic"), obj.getString("Status"));
                        interested.setBookingId(obj.getString("Bookingid"));
                        mList.add(interested);
                    }
                    Log.d("AvailableRides", "Total available Rides : "+mList.size());
                    if(mList.size() == 0){
                        findViewById(R.id.empty).setVisibility(View.VISIBLE);
                        ((MyTextView)findViewById(R.id.empty_txt)).setText("No requests");
                    }else{
                        mAdapter.notifyDataSetChanged();
                    }
                }catch (JSONException ex){
                    findViewById(R.id.empty).setVisibility(View.VISIBLE);
                    ((MyTextView)findViewById(R.id.empty_txt)).setText("No requests");
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(ActivityRideDetail.this, "Networking error", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
