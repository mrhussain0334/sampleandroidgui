package com.novelsol.hamrahi.Rider;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.novelsol.hamrahi.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RiderConfirmation extends Fragment {


    public RiderConfirmation() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rider_confirmation, container, false);
    }

}
