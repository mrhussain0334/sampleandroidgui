package com.novelsol.hamrahi.BroadcastReceivers;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Notification;

import java.util.HashMap;

/**
 * Created by ccn on 09/08/2017.
 */

public class RequestAcceptReceiver extends IntentService {

    public RequestAcceptReceiver() {
        super("Accept");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Notification notification = (Notification)intent.getSerializableExtra("notification");
        callNetwork(getApplicationContext(), Utils.CONFIRM, notification);
    }

    public void callNetwork(final Context context, String url, Notification notification){
        HashMap<String, String> params = new HashMap<>();

        params.put("id", notification.getBookingId());
        params.put("userid", notification.getTargetUserId());
        params.put("ownerid", notification.getSenderUserId());
        params.put("type", notification.getType());

        HTTP.makeHttpPostRequest(context, url, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    Toast.makeText(context, "Ride has been accepted", Toast.LENGTH_LONG).show();
                }else{
                    Log.d("Notification", response);
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, "Networking error", Toast.LENGTH_LONG).show();
            }
        });

    }
}
