package com.novelsol.hamrahi.Driver;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.novelsol.hamrahi.Adapters.ConfirmationAdapter;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Interested;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.MyTextView;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class DriverConfirmation extends Fragment {


    RecyclerView mRecyclerView;
    ConfirmationAdapter mConfirmationAdapter;
    ArrayList<Interested> mList = new ArrayList<>();
    PreferencesHelper mHelper;
    ProgressDialogFragment mProgress;
    SwipeRefreshLayout mSwipe;
    View view;

    public DriverConfirmation() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_driver_confirmation, container, false);
        final Bundle bundle = getArguments();
        mSwipe = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayout);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callNetwork(bundle.getString("type"));
            }
        });
        mProgress = new ProgressDialogFragment();
        getActivity().setTitle("Confirmation");
        mHelper = new PreferencesHelper(getContext());
        mConfirmationAdapter = new ConfirmationAdapter(mList, getActivity());
        mConfirmationAdapter.setCallback(new ConfirmationAdapter.CB() {
            @Override
            public void onClick(int action) {
                callNetwork(bundle.getString("type"));
            }
        });
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerview);
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(mConfirmationAdapter);
        callNetwork(bundle.getString("type"));
        this.view = view;
        return view;
    }

    void callNetwork(String type){
        HashMap<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("userid", mHelper.getUserId());
        params.put("status", "P");
        Log.v("POSTDETAILS", "id = "+mHelper.getUserId()+", status = "+params.get("status")+" , type = "+type);
        mProgress.setTextMessage("Loading..");
        mProgress.show(getActivity().getSupportFragmentManager(), "P");
        HTTP.makeHttpPostRequest(getContext(), Utils.ALL_REQUESTS_REQUESTER, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                JSONArray array;
                mProgress.dismiss();
                mList.clear();
                try{
                    array = new JSONArray(response);
                    for(int i=0; i<array.length(); i++){
                        JSONObject obj = array.getJSONObject(i);
                        Interested interested = new Interested(obj.getString("Userid"), obj.getString("Name"), obj.getString("Phone"), obj.getString("ProfilePic"), obj.getString("Status"));
                        interested.setTo(obj.getString("Destination"));
                        interested.setFrom(obj.getString("StartFrom"));
                        interested.setTime(obj.getString("LeavingTime"));
                        interested.setBookingId(obj.getString("Bookingid"));
                        interested.setType("Type");
                        mList.add(interested);
                    }
                    mConfirmationAdapter.notifyDataSetChanged();
                    if(mList.size()==0){
                        view.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                        ((MyTextView)view.findViewById(R.id.empty_txt)).setText("No results");
                    }else{
                        if(view.findViewById(R.id.empty).getVisibility()==View.VISIBLE){
                            view.findViewById(R.id.empty).setVisibility(View.INVISIBLE);
                        }
                    }
                    if(mSwipe.isRefreshing()){
                        mSwipe.setRefreshing(false);
                    }
                    Log.v("Requests", response);
                }catch (JSONException ex){
                    if(mSwipe.isRefreshing()){
                        mSwipe.setRefreshing(false);
                    }
                    view.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                    ((MyTextView)view.findViewById(R.id.empty_txt)).setText("No results");
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getContext(), "Networking error", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
            }
        });
    }

}
