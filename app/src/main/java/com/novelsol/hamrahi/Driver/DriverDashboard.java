package com.novelsol.hamrahi.Driver;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.ActionMenuView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mypopsy.widget.FloatingSearchView;
import com.novelsol.hamrahi.Adapters.PlaceAutocompleteAdapter;
import com.novelsol.hamrahi.Adapters.SearchAdapter;
import com.novelsol.hamrahi.AvailableRideActivity;
import com.novelsol.hamrahi.Fragments.Requests;
import com.novelsol.hamrahi.Fragments.ScheduledContainer;
import com.novelsol.hamrahi.Fragments.VehicleFragment;
import com.novelsol.hamrahi.Hamrahi;
import com.novelsol.hamrahi.Helper.CustomSuggestionItemAnimator;
import com.novelsol.hamrahi.Helper.GeocoderAsyncTask;
import com.novelsol.hamrahi.Helper.LocationHelper;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.MainActivity;
import com.novelsol.hamrahi.Model.PostInfo;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.RideRequestActivity;
import com.tapadoo.alerter.Alerter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Created by ccn on 22/08/2017.
 */

public class DriverDashboard extends Fragment implements GoogleApiClient.OnConnectionFailedListener, RoutingListener, GeocoderAsyncTask.ICallback {

    FloatingSearchView mSearchView;

    SearchAdapter mAdapter;
    GoogleApiClient mGoogleApiClient;

    View mLayout;
    TextView mTime;
    Button mConfirm;
    private GoogleMap mMap;
    Location location;
    String time;
    Button mPoint;
    MapView mMapView;
    EditText mPointerEditText;
    boolean timePicked = false;
    boolean locationPicked = false;
    private static final int LOGIN_AS_DRIVER = 0;
    private static final int LOIGN_AS_RIDER = 1;
    PreferencesHelper mHelper;
    Context context;
    ProgressBar mPointerProgress;
    View pointerView;
    View dropPinView;
    boolean isPinModeSelected = false;
    private static final LatLngBounds BOUNDS = new LatLngBounds(
            new LatLng(31.401422, 74.211078), new LatLng(31.554606, 74.357158));
    FloatingActionButton mGo;
    public DriverDashboard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_rider_dashboard, container, false);
        //if(Hamrahi.getGoogleApiHelper().isConnected()) {
        mGoogleApiClient= Hamrahi.getGoogleApiHelper().getGoogleApiClient();
        mHelper = new PreferencesHelper(context);
        mPoint = (Button)view.findViewById(R.id.point_to_my_loc);
        mMapView = (MapView)view.findViewById(R.id.mapView);
        mGo = (FloatingActionButton)view.findViewById(R.id.go);
        mPointerEditText = (EditText)view.findViewById(R.id.pointerEditText);
        mPointerProgress = (ProgressBar)view.findViewById(R.id.pointer_progress);
        mGo.hide();
        dropPinView = view.findViewById(R.id.drop_pin);

        mPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mStart != null){
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mStart, mMap.getCameraPosition().zoom));
                }else{
                    Toast.makeText(getContext(), "Location is loading", Toast.LENGTH_LONG).show();
                }
            }
        });
        pointerView = view.findViewById(R.id.pointerView);
        getActivity().setTitle("Dashboard");
        mSearchView = (FloatingSearchView)view. findViewById(R.id.search);
        mAdapter = new SearchAdapter(context, mGoogleApiClient, BOUNDS, null);
        mSearchView.setAdapter(mAdapter);
        mSearchView.showLogo(true);
        mSearchView.setItemAnimator(new CustomSuggestionItemAnimator(mSearchView));
        mLayout = view.findViewById(R.id.time_container);
        mConfirm = (Button)view.findViewById(R.id.confirm);
        mTime = (TextView)view.findViewById(R.id.time);

        SimpleDateFormat sdf = new SimpleDateFormat("HH : mm aa");
        mTime.setText(sdf.format(new Date()));

        mSearchView.setOnIconClickListener(new FloatingSearchView.OnIconClickListener() {
            @Override
            public void onNavigationClick() {
                // toggle
                mSearchView.setActivated(!mSearchView.isActivated());
            }
        });

        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSearchAction(CharSequence text) {
                mSearchView.setActivated(false);
            }
        });

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!timePicked){
                    Alerter.create(getActivity())
                            .setTitle("Alert")
                            .setText("Please pick the time first")
                            .setBackgroundColorRes(R.color.colorAccent)
                            .enableSwipeToDismiss()
                            .show();
                }else{
                    if(locationPicked){
                        mSearchView.setVisibility(GONE);
                        pointerView.setVisibility(VISIBLE);
                        mPoint.setVisibility(GONE);
                        mLayout.setVisibility(INVISIBLE);
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mEnd, 16));
                        dropPinView.setVisibility(VISIBLE);
                        isPinModeSelected = true;
                        mMap.clear();
                        mGo.show();
                    }else{
                        Toast.makeText(getContext(), "Please pick a location First", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        mGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocationNameByLatLng(mStart, 1);
                getLocationNameByLatLng(mEnd, 2);
            }
        });


        view.findViewById(R.id.time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickTime(v);
            }
        });


        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence query, int start, int before, int count) {
                showClearButton(query.length() > 0 && mSearchView.isActivated());
                mAdapter.clear();
                mAdapter.getFilter().filter(query);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mSearchView.setOnSearchFocusChangedListener(new FloatingSearchView.OnSearchFocusChangedListener() {
            @Override
            public void onFocusChanged(final boolean focused) {
                boolean textEmpty = mSearchView.getText().length() == 0;

                showClearButton(focused && !textEmpty);
                if(!focused) showProgressBar(false);
                mSearchView.showLogo(!focused && textEmpty);

                if (focused) {
                    mSearchView.showIcon(true);
                    showProgressBar(true);
                }else{
                    mSearchView.showIcon(false);
                }
            }
        });

        mAdapter.setOnPlaceSelectionListener(new PlaceAutocompleteAdapter.PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(final AutocompletePrediction place) {

                mSearchView.setActivated(false);

                new android.os.AsyncTask<Void, Void, Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        Places.GeoDataApi.getPlaceById(mGoogleApiClient, place.getPlaceId())
                                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                    @Override
                                    public void onResult(PlaceBuffer places) {
                                        if (places.getStatus().isSuccess()) {
                                            final Place myPlace = places.get(0);
                                            LatLng loc = myPlace.getLatLng();
                                            mSearchView.setText(myPlace.getName()+", "+myPlace.getAddress());
                                            if(location !=null){
                                                getRoute(new LatLng(location.getLatitude(), location.getLongitude()), loc);
                                            }else{
                                                Toast.makeText(getContext(), "Current location is not available", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        locationPicked = true;
                                        places.release();
                                    }
                                });
                        return null;
                    }
                }.execute();
            }
        });

        mSearchView.setOnSearchFocusChangedListener(new FloatingSearchView.OnSearchFocusChangedListener() {
            @Override
            public void onFocusChanged(boolean focused) {
                if(focused){
                    mLayout.setVisibility(INVISIBLE);
                }
            }
        });


        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);

                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    //return v;
                }
                if(mGoogleApiClient.isConnected()){
                    location = LocationHelper.getInstance().getLocation(getActivity().getApplication(), mGoogleApiClient);
                    if(location!=null){
                        updateLocation(location);
                        mStart = new LatLng(location.getLatitude(), location.getLongitude());
                    }else{
                        Toast.makeText(getContext(), "Cannot obtain current location", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    mGoogleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            location = LocationHelper.getInstance().getLocation(context, mGoogleApiClient);
                            if(location!=null){
                                updateLocation(location);
                                mStart = new LatLng(location.getLatitude(), location.getLongitude());
                            }else{
                                Toast.makeText(getContext(), "Cannot obtain current location", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    });
                }

                mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                        //get latlng at the center by calling
                        if(isPinModeSelected){
                            mEnd = mMap.getCameraPosition().target;
                            mPointerProgress.setVisibility(VISIBLE);
                            getLocationNameByLatLng(mEnd, 0);
                        }
                    }
                });

            }
        });

        try {
            MapsInitializer.initialize(context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        try{
            if(mGoogleApiClient.isConnected()){
                location = LocationHelper.getInstance().getLocation(context, mGoogleApiClient);
                if(location == null){
                    Toast.makeText(getContext(), "Cannot obtain current location", Toast.LENGTH_SHORT).show();
                }else{
                    updateLocation(location);
                }
            }else {
                mGoogleApiClient.reconnect();
                location = LocationHelper.getInstance().getLocation(context, mGoogleApiClient);
                if(location == null){
                    Toast.makeText(getContext(), "Cannot obtain current location", Toast.LENGTH_SHORT).show();
                }else{
                    updateLocation(location);
                }
            }
        }catch (Exception ex){
            mGoogleApiClient.reconnect();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            switch (resultCode){
                case Utils.PROFILE_MSG_SENT_REQUESTS:{
                    Requests newFragment = new Requests();
                    Bundle bundle = new Bundle();
                    bundle.putString("type", mHelper.getLoginAs()==LOGIN_AS_DRIVER?"drives":"rides");
                    newFragment.setArguments(bundle);
                    transaction.replace(R.id.container, newFragment);
                    transaction.commit();
                    break;
                }case Utils.PROFILE_MY_POSTS:{
                    ScheduledContainer newFragment = new ScheduledContainer();
                    Bundle bundle = new Bundle();
                    bundle.putString("type", mHelper.getLoginAs()==LOGIN_AS_DRIVER?"drives":"rides");
                    newFragment.setArguments(bundle);
                    transaction.replace(R.id.container, newFragment);
                    transaction.commit();
                    break;
                }case Utils.ADD_VEHICLE:{
                    VehicleFragment newFragment = new VehicleFragment();
                    transaction.replace(R.id.container, newFragment);
                    transaction.commit();
                }
            }
    }

    private void updateLocation(Location location) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));
    }

    private void showProgressBar(boolean show) {
        //mSearchView.getMenu().findItem(R.id.menu_progress).setVisible(show);
    }


    private void showClearButton(boolean show) {
        //mSearchView.getMenu().findItem(R.id.menu_clear).setVisible(show);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(context, "Connection failed",Toast.LENGTH_SHORT).show();
    }

    public void pickTime(View view){

        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        SimpleDateFormat S = new SimpleDateFormat("yyyy-MM-dd");

                        String date = S.format(c.getTime());

                        String ma = hourOfDay>=12?"PM":"AM";

                        time = date+" "+hourOfDay+":"+minute+":00";

                        hourOfDay = hourOfDay>=12?hourOfDay-12:hourOfDay;

                        String h = String.valueOf(hourOfDay).length()==1?"0"+String.valueOf(hourOfDay):String.valueOf(hourOfDay);
                        String m = String.valueOf(minute).length()==1?"0"+String.valueOf(minute):String.valueOf(minute);

                        mTime.setText(h+" : "+m+" "+ma);

                        timePicked = true;

                        Log.v("time", time);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    ProgressDialog progressDialog;

    LatLng mStart, mEnd;

    void getRoute(LatLng mStart, LatLng mEnd){

        this.mStart = mStart;
        this.mEnd = mEnd;

        progressDialog = ProgressDialog.show(context, "Please wait.",
                "Fetching route information.", true);
        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .waypoints(mStart, mEnd)
                .build();
        routing.execute();
    }



    @Override
    public void onRoutingFailure(RouteException e) {
        progressDialog.dismiss();
        Toast.makeText(context, "Error while getting route", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRoutingStart() {

    }

    ArrayList<Polyline> polylines;

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int j) {
        progressDialog.dismiss();
        CameraUpdate center = CameraUpdateFactory.newLatLng(mStart);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);

        mMap.moveCamera(center);
        mMap.moveCamera(zoom);
        mMap.clear();
        mMap.addMarker(new MarkerOptions()
                .position(mStart)
                .title("Your location"));

        mMap.addMarker(new MarkerOptions()
                .position(mEnd)
                .title("Your destination"));

        polylines = new ArrayList<>();

        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();

        for (int i = 0; i < route.size(); i++) {
            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(ResourcesCompat.getColor(getResources(), R.color.colorPrimaryDark, null));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);
        }
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(mEnd).include(mStart);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(builder.build().getCenter(), 11));
        mLayout.setVisibility(VISIBLE);

    }

    @Override
    public void onRoutingCancelled() {
        progressDialog.dismiss();
    }

    void getLocationNameByLatLng(LatLng mLatLng, int requestType){
        GeocoderAsyncTask task = new GeocoderAsyncTask();
        task.setCallback(this, requestType);
        task.context = getContext();
        task.execute(mLatLng);
    }

    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);
    }

    String start;
    String dest;

    @Override
    public void onResult(String address, int requestType) {
        if(requestType == 0){
            mPointerEditText.setText(address);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mPointerProgress.setVisibility(GONE);
                }
            });

        }else if(requestType == 1){
            start = address;
            if(dest!=null){
                postDrive();
            }
        }else if(requestType == 2){
            dest = address;
            if(start!=null){
                postDrive();
            }
        }
    }

    void postDrive(){
        Intent intent = new Intent(context, RideRequestActivity.class);
        PostInfo post = new PostInfo();

        post.setStarLon(mStart.longitude);
        post.setStartLat(mStart.latitude);
        post.setEndLat(mEnd.latitude);
        post.setEndLon(mEnd.longitude);

        post.setStartAddress(start);
        post.setEndAddress(dest);
        post.setLeavingTime(time);
        intent.putExtra("post", post);
        startActivityForResult(intent, 0);
    }
}
