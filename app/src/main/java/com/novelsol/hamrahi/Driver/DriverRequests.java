package com.novelsol.hamrahi.Driver;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.novelsol.hamrahi.Adapters.RequestsAdapter;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Interested;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.MyTextView;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class DriverRequests extends Fragment {

    private static final String TAG = "AllRequests";
    ProgressDialogFragment mProgress;


    RecyclerView mRecyclerView;
    RequestsAdapter mRequestsAdapter;
    SwipeRefreshLayout mSwipe;
    ArrayList<Interested> mList = new ArrayList<>();
    PreferencesHelper mHelper;
    View view;

    public DriverRequests() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_driver_requests, container, false);
        final Bundle bundle = getArguments();
        mSwipe = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayout);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callNetwork(bundle.getString("type"));
            }
        });
        mProgress = new ProgressDialogFragment();
        mProgress.setTextMessage("Loading..");
        mHelper = new PreferencesHelper(getContext());
        mRequestsAdapter = new RequestsAdapter(mList, getActivity(), bundle);
        mRequestsAdapter.setCallback(new RequestsAdapter.CB() {
            @Override
            public void onClick(int action) {
                callNetwork(bundle.getString("type"));
            }
        });
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(mRequestsAdapter);
        callNetwork(bundle.getString("type"));
        this.view = view;
        return view;
    }

    void callNetwork(String type){
        mProgress.show(getActivity().getSupportFragmentManager(), "P");
        Log.v(TAG, type);
        Log.v(TAG, mHelper.getUserId());
        HashMap<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("userid", mHelper.getUserId());
        params.put("status", "I");
        HTTP.makeHttpPostRequest(getContext(), Utils.ALL_REQUESTS, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                JSONArray array;
                mProgress.dismiss();
                mList.clear();
                try{
                    array = new JSONArray(response);
                    for(int i=0; i<array.length(); i++){
                        JSONObject obj = array.getJSONObject(i);
                        Interested interested = new Interested(obj.getString("Userid"), obj.getString("Name"), obj.getString("Phone"), obj.getString("ProfilePic"), obj.getString("Status"));
                        interested.setTo(obj.getString("Destination"));
                        interested.setFrom(obj.getString("StartFrom"));
                        interested.setTime(obj.getString("LeavingTime"));
                        interested.setBookingId(obj.getString("Bookingid"));
                        mList.add(interested);
                    }

                    Log.d("AvailableRides", "Total available Rides : "+mList.size());

                    if(mList.size()==0){
                        view.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                        ((MyTextView)view.findViewById(R.id.empty_txt)).setText("No requests");
                    }else{
                        if(view.findViewById(R.id.empty).getVisibility()==View.VISIBLE){
                            view.findViewById(R.id.empty).setVisibility(View.INVISIBLE);
                        }
                    }
                    mRequestsAdapter.notifyDataSetChanged();
                    if(mSwipe.isRefreshing()){
                        mSwipe.setRefreshing(false);
                    }
                    Log.v("Requests", response);
                }catch (JSONException ex){
                    if(mSwipe.isRefreshing()){
                        mSwipe.setRefreshing(false);
                    }
                    view.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                    ((MyTextView)view.findViewById(R.id.empty_txt)).setText("No requests");
                    //Toast.makeText(DriverRequests.this.getContext(), response, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getContext(), "Networking error", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
            }
        });
    }

}
