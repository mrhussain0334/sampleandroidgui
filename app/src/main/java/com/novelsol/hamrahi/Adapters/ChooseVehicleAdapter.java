package com.novelsol.hamrahi.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.novelsol.hamrahi.Model.Vehicle;
import com.novelsol.hamrahi.R;

import java.util.ArrayList;

/**
 * Created by Shahzaib on 9/8/2017.
 */

public class ChooseVehicleAdapter extends BaseAdapter {

    ArrayList<Vehicle> mList;

    private int checked = -1;

    public ChooseVehicleAdapter(ArrayList<Vehicle> mList){
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(convertView.getContext());
        View row = inflater.inflate(R.layout.choose_vehicle_list_item, parent, false);
        Vehicle vehicle = mList.get(position);
        View hover = row.findViewById(R.id.hover);
        hover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                checked = position;
                notifyDataSetChanged();
            }
        });

        TextView mName = (TextView)row.findViewById(R.id.name);
        ImageView mThumb = (ImageView)row.findViewById(R.id.thumb);
        CheckBox mCheckBox = (CheckBox)row.findViewById(R.id.checkbox);
        if(position==checked){
            mCheckBox.setChecked(true);
        }

        mName.setText(vehicle.getRegNo());
        mThumb.setImageResource(vehicle.getVehicleType().equalsIgnoreCase("car")?R.drawable.ic_car:R.drawable.ic_bike);
        return row;
    }

    int getChecked(){
        return checked;
    }
}
