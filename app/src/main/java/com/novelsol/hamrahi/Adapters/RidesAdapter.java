package com.novelsol.hamrahi.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Ride;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.ProfileDialogFragment;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;
import com.novelsol.hamrahi.ui.TeenRegularTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;

/**
 * Created by hhala on 5/29/2017.
 */
public class RidesAdapter extends RecyclerView.Adapter<RidesAdapter.MyViewHolder> {

    List<Ride> rideList;
    Context mContext;
    PreferencesHelper mHelper;
    String type;
    AppCompatActivity activity;
    ProgressDialogFragment mProgress;
    CB1 mCallback;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(type.equalsIgnoreCase("R")?R.layout.item_ride:R.layout.item_drive, parent, false);
        return new MyViewHolder(itemView);
    }

    public void setCB1(CB1 mCallback){
        this.mCallback = mCallback;
    }

    public RidesAdapter(List<Ride> rideList, Context mContext, String type) {
        this.rideList = rideList;
        this.mContext = mContext;
        this.type = type;
        activity = (AppCompatActivity)mContext;
        mHelper = new PreferencesHelper(mContext);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Ride ride = rideList.get(position);

        holder.mFrom.setText(ride.getStart());
        holder.mTo.setText(ride.getDestination());
        holder.mUserName.setText(ride.getName());
        if(!type.equalsIgnoreCase("R")){
            holder.mAvailableSeats.setText(ride.getSeats());
            holder.mVehicleName.setText(ride.getVehicleName());
            holder.mVehicleNumber.setText(ride.getVehicleNumber());
        }
        try {
            holder.mLeavingTime.setText(new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(ride.getLeavingTime())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.mInterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgress = new ProgressDialogFragment();
                mProgress.setTextMessage("Please wait..");
                mProgress.show(activity.getSupportFragmentManager(), "P");
                callNetwork(ride, position);
            }
        });

        Glide.with(mContext).load(ride.getPicture()).error(R.drawable.ic_user_avatar).centerCrop().fitCenter().into(holder.mUserImg);

    }

    private void callNetwork(final Ride ride, final int position) {
        HashMap<String, String > params = new HashMap<>();
        params.put("userid", mHelper.getUserId());
        params.put("postid", ride.getPostId());
        params.put("type", type);

        HTTP.makeHttpPostRequest(mContext, Utils.INTERESTED, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    mProgress.dismiss();
                    Bundle bundle = new Bundle();
                    bundle.putString("pic", ride.getPicture());
                    bundle.putString("name", ride.getName());
                    bundle.putString("phone", ride.getPhone());
                    bundle.putDouble("long", ride.getStartLoc().longitude);
                    bundle.putDouble("lat", ride.getStartLoc().latitude);
                    ProfileDialogFragment d = new ProfileDialogFragment();
                    d.setCallBack(new ProfileDialogFragment.ClickCallback() {
                        @Override
                        public void onClick(int mode) {
                            switch (mode){
                                case Utils.PROFILE_INTERESTED_ONLY:{
                                    mCallback.onClick(Utils.PROFILE_INTERESTED_ONLY);
                                    break;
                                }case Utils.POST_RIDE:{
                                    mCallback.onClick(Utils.POST_RIDE);
                                    break;
                                }case Utils.PROFILE_MSG_SENT_REQUESTS:{
                                    Intent intent = new Intent();
                                    intent.putExtra("action", Utils.PROFILE_MSG_SENT_REQUESTS);
                                    activity.setResult(Utils.PROFILE_MSG_SENT_REQUESTS, intent);
                                    activity.finish();
                                    break;
                                }case Utils.PROFILE_MY_POSTS:{
                                    Intent intent = new Intent();
                                    intent.putExtra("action", Utils.PROFILE_MY_POSTS);
                                    activity.setResult(Utils.PROFILE_MY_POSTS, intent);
                                    activity.finish();
                                    break;
                                }
                            }
                        }
                    });
                    FragmentActivity activity = (FragmentActivity) mContext;
                    d.setArguments(bundle);
                    d.show(activity.getSupportFragmentManager(), "Profile");
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return rideList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView mUserImg;
        TeenRegularTextView mUserName;
        TextView mLeavingTime;
        TextView mAvailableSeats;
        ImageView mVehicleType;
        TextView mTo;
        TextView mFrom;
        TextView mInterested;
        TextView mVehicleName;
        TextView mVehicleNumber;

        public MyViewHolder(View itemView) {
            super(itemView);
            mUserImg = (CircleImageView) itemView.findViewById(R.id.user_img);
            mUserName = (TeenRegularTextView)itemView.findViewById(R.id.user_name);
            mLeavingTime = (TextView)itemView.findViewById(R.id.time);
            mAvailableSeats = (TextView)itemView.findViewById(R.id.available_seats);
            mVehicleType = (ImageView)itemView.findViewById(R.id.vehicle_type);
            mTo = (TextView)itemView.findViewById(R.id.to);
            mFrom = (TextView)itemView.findViewById(R.id.from);
            mInterested = (TextView)itemView.findViewById(R.id.interested);
            mVehicleName = (TextView)itemView.findViewById(R.id.vehicle_name);
            mVehicleNumber = (TextView)itemView.findViewById(R.id.vehicle_number);
        }
    }
    public interface CB1{
        void onClick(int action);
    }
}
