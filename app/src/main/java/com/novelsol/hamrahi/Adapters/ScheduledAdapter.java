package com.novelsol.hamrahi.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter;
import com.novelsol.hamrahi.ActivityRideDetail;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Model.Scheduled;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.RatingInputDialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ccn on 27/07/2017.
 */

public class ScheduledAdapter extends SectionRecyclerViewAdapter<SectionHeader, Scheduled, ScheduledAdapter.SectionViewHolder, ScheduledAdapter.MyViewHolder> {

    ArrayList<Scheduled> mList = new ArrayList<>();
    Bundle bundle;
    Context context;
    PreferencesHelper mHelper;

    private static final int LOGIN_AS_DRIVER = 0;
    private static final int LOGIN_AS_RIDER = 1;

    public ScheduledAdapter(Context context, List<SectionHeader> sectionItemList, Bundle bundle){
        super(context, sectionItemList);
        this.mList = mList;
        this.bundle = bundle;
        this.context = context;
        mHelper = new PreferencesHelper(context);
    }

    @Override
    public SectionViewHolder onCreateSectionViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.section_header, viewGroup, false);
        return new SectionViewHolder(view);
    }

    @Override
    public MyViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(childViewGroup.getContext()).inflate(R.layout.schduled_booking_item, childViewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(SectionViewHolder sectionViewHolder, int sectionPosition, SectionHeader section) {
        sectionViewHolder.name.setText(section.sectionText);
    }

    @Override
    public void onBindChildViewHolder(final MyViewHolder holder, final int sectionPosition, final int childPosition, final Scheduled obj) {

        if (obj.getSection() == 0) {
            holder.mMarkCompleted.setVisibility(View.VISIBLE);
            holder.mMarkCompleted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RatingInputDialogFragment rating = new RatingInputDialogFragment();
                    AppCompatActivity activity = (AppCompatActivity) context;
                    FragmentManager manager = activity.getSupportFragmentManager();
                    Bundle bundle = new Bundle();
                    rating.setReviewPostCallback(new RatingInputDialogFragment.ReviewPostCallback() {
                        @Override
                        public void onReviewPosted() {
                            removeChild(sectionPosition, childPosition);
                            Toast.makeText(context, "Hey the review has been posted", Toast.LENGTH_SHORT).show();
                        }
                    });
                    bundle.putString("postid", obj.getBookingId());
                    bundle.putString("type", obj.getType());
                    bundle.putString("userid", mHelper.getUserId());
                    rating.setArguments(bundle);
                    rating.show(manager, "R");
                }
            });
            holder.mInterestedView.setVisibility(View.INVISIBLE);
            try {
                String time = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(obj.getTime()));
                String mr = time.substring(5, time.length() - 1);
                holder.mLeavingTime.setText(time.split(" ")[0]);
                holder.mr.setText(mr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.mTo.setText(obj.getTo());
            holder.mFrom.setText(obj.getFrom());

        } else {
            holder.mInterestedPeople.setText(obj.getInterested());
            try {
                String time = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(obj.getTime()));
                String mr = time.substring(5, time.length() - 1);
                holder.mLeavingTime.setText(time.split(" ")[0]);
                holder.mr.setText(mr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.mTo.setText(obj.getTo());
            holder.mFrom.setText(obj.getFrom());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.mMarkCompleted.getVisibility() != View.VISIBLE) {
//                    Intent intent = new Intent(context, ActivityRideDetail.class);
//                    intent.putExtra("obj", obj);
//                    context.startActivity(intent);
                }
            }
        });
    }

    private void callNetwork(String url, String id) {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", id);
        HTTP.makeHttpPostRequest(context, url, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                Toast.makeText(context, "Post deleted", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, "Error : Post could not be deleted", Toast.LENGTH_SHORT).show();
            }
        });
    }


    class MyViewHolder extends RecyclerView.ViewHolder{

        //View mCancel, mShowInterestedPeople, mConfirmCompleted;
        TextView mBookingId;
        TextView mLeavingTime;
        TextView mFrom, mTo, mr;
        Button mMarkCompleted;
        View mInterestedView;

        View itemView;

        CircleImageView mPic;
        public TextView mInterestedPeople;

        MyViewHolder(View view){
            super(view);
            this.itemView = view;
           mFrom = (TextView)view.findViewById(R.id.from);
            mTo = (TextView)view.findViewById(R.id.to);
            mBookingId = (TextView)view.findViewById(R.id.booking);
            mLeavingTime = (TextView)view.findViewById(R.id.time);
            mPic = (CircleImageView)view.findViewById(R.id.pic);
            mInterestedPeople= (TextView)view.findViewById(R.id.interested);
            mr = (TextView)view.findViewById(R.id.mr);
            mMarkCompleted = (Button)itemView.findViewById(R.id.mark_completed);
            mInterestedView = view.findViewById(R.id.interested_view);
        }
    }

    public class SectionViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        public SectionViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.section);
        }
    }

}