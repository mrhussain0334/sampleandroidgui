package com.novelsol.hamrahi.Adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Ride;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.ProfileDialogFragment;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;
import com.novelsol.hamrahi.ui.TeenRegularTextView;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shahzaib on 7/16/2017.
 */

public class RideRequestsAdapter extends RecyclerView.Adapter<RideRequestsAdapter.MyViewHolder>{
    List<Ride> rideList;
    PreferencesHelper mHelper;
    Context mContext;
    ProgressDialogFragment mProgress;
    AppCompatActivity activity;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_request_ride, parent, false);

        return new MyViewHolder(itemView);
    }

    public RideRequestsAdapter(List<Ride> rideList, Context mContext) {
        this.rideList = rideList;
        this.mContext = mContext;
        mHelper = new PreferencesHelper(mContext);
        activity = (AppCompatActivity)mContext;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Ride ride = rideList.get(position);

        holder.mFrom.setText(ride.getStart());
        holder.mTo.setText(ride.getDestination());
//        holder.mVehicleType.setImageResource(ride.getType().equals("car")?R.drawable.ic_car_type:R.drawable.ic_bicycle);
        holder.mUserName.setText(ride.getName());
        //holder.mUserImg.setImageResource(ride.getImage());
        holder.mLeavingTime.setText(ride.getLeavingTime());

        //holder.mPhone.setText(ride.getPhone());

//        holder.mPhone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ride.getPhone()));
//                if (ActivityCompat.checkSelfPermission(holder.mPhone.getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    return;
//                }
//                holder.mPhone.getContext().startActivity(intent);
//            }
//        });
        holder.mInterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgress = new ProgressDialogFragment();
                mProgress.setTextMessage("Please wait");
                mProgress.show(activity.getSupportFragmentManager(), "P");
                callNetwork(ride);
            }
        });

        Glide.with(mContext).load(ride.getPicture()).error(R.drawable.ic_user_avatar).centerCrop().fitCenter().into(holder.mUserImg);

    }

    private void callNetwork(Ride ride) {
        HashMap<String, String > params = new HashMap<>();
        params.put("userid", mHelper.getUserId());
        params.put("postid", ride.getPostId());
        params.put("type", Utils.TYPE_RIDER);

        HTTP.makeHttpPostRequest(mContext, Utils.INTERESTED, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    mProgress.dismiss();
                    ProfileDialogFragment d = new ProfileDialogFragment();
                    d.show(activity.getSupportFragmentManager(), "Profile");
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return rideList.size();
    }






    class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView mUserImg;
        TeenRegularTextView mUserName;
        TextView mLeavingTime;
        TextView mAvailableSeats;
        ImageView mVehicleType;
        TextView mTo;
        TextView mFrom;
        TextView mInterested;

        public MyViewHolder(View itemView) {
            super(itemView);
            mUserImg = (CircleImageView) itemView.findViewById(R.id.user_img);
            mUserName = (TeenRegularTextView)itemView.findViewById(R.id.user_name);
            mLeavingTime = (TextView)itemView.findViewById(R.id.time);
            mAvailableSeats = (TextView)itemView.findViewById(R.id.available_seats);
            mVehicleType = (ImageView)itemView.findViewById(R.id.vehicle_type);
            mTo = (TextView)itemView.findViewById(R.id.to);
            mFrom = (TextView)itemView.findViewById(R.id.from);
            mInterested = (TextView)itemView.findViewById(R.id.interested);
        }
    }
}
