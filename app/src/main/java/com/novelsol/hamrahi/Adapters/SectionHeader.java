package com.novelsol.hamrahi.Adapters;

import com.intrusoft.sectionedrecyclerview.Section;
import com.novelsol.hamrahi.Model.Scheduled;

import java.util.List;

/**
 * Created by shahz on 10/14/2017.
 */

public class SectionHeader implements Section<Scheduled> {

    List<Scheduled> childList;
    String sectionText;

    public SectionHeader(List<Scheduled> childList, String sectionText) {
        this.childList = childList;
        this.sectionText = sectionText;
    }

    @Override
    public List<Scheduled> getChildItems() {
        return childList;
    }

    public String getSectionText() {
        return sectionText;
    }
}
