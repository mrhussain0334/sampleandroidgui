package com.novelsol.hamrahi.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Interested;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.MyTextView;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shahzaib on 7/27/2017.
 */

public class InterestedPeopleAdapter extends RecyclerView.Adapter<InterestedPeopleAdapter.MyViewHolder> {

    ArrayList<Interested> mList;
    Bundle mIntent;
    PreferencesHelper mHelper;
    Context mContext;

    public InterestedPeopleAdapter(Context mContext, ArrayList<Interested> mList, Bundle mIntent){
        this.mList = mList;
        this.mIntent = mIntent;
        this.mContext = mContext;
        mHelper = new PreferencesHelper(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.interested_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Interested obj = mList.get(position);
        holder.mName.setText(obj.getName());
        holder.mPhone.setText(obj.getPhone());
        holder.mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reject(obj);
                mList.remove(position);
                notifyItemRemoved(position);
            }
        });
        holder.mAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accept(obj);
                mList.remove(position);
                notifyItemRemoved(position);
            }
        });
        if(obj.getStatus().equalsIgnoreCase("c")){
            holder.mCancel.setOnClickListener(null);
            holder.mAccept.setOnClickListener(null);
            holder.mConfirmed.setVisibility(View.VISIBLE);
        }
        Glide.with(mContext).load(obj.getPicture()).error(R.drawable.ic_user_avatar).fitCenter().centerCrop().into(holder.mProfile);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        MyTextView mName;
        MyTextView mPhone;
        View mCancel, mAccept;
        TextView mConfirmed;
        CircleImageView mProfile;

        public MyViewHolder(View itemView) {
            super(itemView);
            mName = (MyTextView) itemView.findViewById(R.id.name);
            mPhone = (MyTextView) itemView.findViewById(R.id.phone);
            mAccept = itemView.findViewById(R.id.accept);
            mCancel = itemView.findViewById(R.id.cancel);
            mConfirmed = (TextView)itemView.findViewById(R.id.confirmed);
            mProfile = (CircleImageView)itemView.findViewById(R.id.pic);
        }
    }

    private void accept(Interested obj){
        HashMap<String, String> params = new HashMap<>();
        params.put("id", obj.getBookingId());
        params.put("userid", obj.getId());
        params.put("ownerid", mHelper.getUserId());
        params.put("type", mIntent.getString("type"));

        HTTP.makeHttpPostRequest(mContext, Utils.ACCEPT_REQUEST, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    Toast.makeText(mContext, "Request Accepted", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(mContext, "Error Occurred", Toast.LENGTH_SHORT).show();
                }
                Log.d("AcceptRequest", response);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(mContext, "Networking Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void reject(Interested obj){

    }
}
