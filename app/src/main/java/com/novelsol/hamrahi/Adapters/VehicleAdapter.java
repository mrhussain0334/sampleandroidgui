package com.novelsol.hamrahi.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.novelsol.hamrahi.Model.Vehicle;
import com.novelsol.hamrahi.R;

import java.util.List;

/**
 * Created by Shahzaib on 9/8/2017.
 */
public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.VehicleViewHolder> {


    private List<Vehicle> vehicleList;
    private final Context context;
    private OnCardViewClickListener onCardViewClickListener;
    public VehicleAdapter(List<Vehicle> vehicleList, Context context, OnCardViewClickListener onCardViewClickListener) {
        this.vehicleList = vehicleList;
        this.context = context;
        this.onCardViewClickListener = onCardViewClickListener;
    }
    public void add(Vehicle v)
    {
        vehicleList.add(v);
        notifyDataSetChanged();
    }

    public void clear()
    {
        vehicleList.clear();
        notifyDataSetChanged();
    }
    public void setVehicleList(List<Vehicle> e)
    {
        vehicleList = e;
        notifyDataSetChanged();
    }
    @Override
    public VehicleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vehicle,parent,false);
        return new VehicleViewHolder(v);
    }

    @Override
    public void onBindViewHolder(VehicleViewHolder holder, int position) {
        try {
            final Vehicle v = vehicleList.get(position);
            holder.txtReg.setText(v.getRegNo());
            holder.txtMake.setText(v.getMake() + " " + v.getModel());
            if (v.getVehicleType().equalsIgnoreCase("Car"))
            {
                holder.vehicleImageView.setImageResource(R.drawable.ic_car);
            }
            else {
                holder.vehicleImageView.setImageResource(R.drawable.ic_bike);
            }

            holder.vehicleCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCardViewClickListener.onCardViewClick(v);
                }
            });
            holder.vehicleCardView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    onCardViewClickListener.onCardViewLongClick(v);
                    return true;
                }
            });
        }
        catch (Exception e)
        {
            Log.e("Error",e.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return vehicleList.size();
    }

    class VehicleViewHolder extends RecyclerView.ViewHolder{

        public final ImageView vehicleImageView;
        public final TextView txtMake,txtReg;
        public final CardView vehicleCardView;
        public VehicleViewHolder(View itemView) {
            super(itemView);
            vehicleImageView = (ImageView) itemView.findViewById(R.id.vehicleImageView);
            txtMake = (TextView) itemView.findViewById(R.id.txtMake);
            txtReg = (TextView) itemView.findViewById(R.id.txtRegNo);
            vehicleCardView = (CardView) itemView.findViewById(R.id.vehicleCardView);
        }
    }

    public interface OnCardViewClickListener {
        void onCardViewClick(Vehicle v);
        void onCardViewLongClick(Vehicle v);
    }
}
