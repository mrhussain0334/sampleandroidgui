package com.novelsol.hamrahi.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.novelsol.hamrahi.Model.History;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.MyTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Shahzaib on 7/30/2017.
 */

public class RideHistoryAdapter extends RecyclerView.Adapter<RideHistoryAdapter.MyViewHolder> {

    ArrayList<History> mList;

    public RideHistoryAdapter(ArrayList<History> mList){
        this.mList = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int i) {
        History item = mList.get(i);
        holder.mFrom.setText(item.getFrom());
        holder.mTo.setText(item.getTo());
        try {
            holder.mDate.setText(new SimpleDateFormat("MM/dd/yyyy").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(item.getTime())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.mName.setText(item.getDriverName());
        holder.mRating.setText(item.getRating());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView mPicture;
        TextView mName, mDate, mRating, mTo, mFrom;
        public MyViewHolder(View itemView) {
            super(itemView);
            mPicture = (ImageView)itemView.findViewById(R.id.pic);
            mName = (TextView)itemView.findViewById(R.id.name);
            mDate = (TextView)itemView.findViewById(R.id.date);
            mRating = (TextView)itemView.findViewById(R.id.rating);
            mTo = (TextView)itemView.findViewById(R.id.to);
            mFrom = (TextView)itemView.findViewById(R.id.from);
        }
    }
}
