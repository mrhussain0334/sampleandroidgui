package com.novelsol.hamrahi.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.novelsol.hamrahi.Model.Notification;

import java.util.List;


/**
 * Created by ccn on 23/08/2017.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder> {

    List<Notification> mList;

    public NotificationsAdapter(List<Notification> mList){
        this.mList = mList;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        MyViewHolder(View view){
            super(view);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

}
