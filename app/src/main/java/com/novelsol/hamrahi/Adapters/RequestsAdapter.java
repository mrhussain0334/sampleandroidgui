package com.novelsol.hamrahi.Adapters;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Interested;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ccn on 10/09/2017.
 */

public class RequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Interested> mList;
    FragmentActivity mContext;
    PreferencesHelper mHelper;
    Bundle mIntent;
    public static final int REFRESH = 0;
    CB cb;

    public RequestsAdapter(ArrayList<Interested> mList, FragmentActivity mContext, Bundle mIntent){
        this.mList = mList;
        this.mContext = mContext;
        this.mIntent = mIntent;
        mHelper = new PreferencesHelper(mContext);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_requests_received, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, final int position) {
        if(holder1 instanceof  MyViewHolder){
            final Interested item = mList.get(position);
            MyViewHolder holder = (MyViewHolder)holder1;
            Glide.with(mContext).load(item.getPicture()).error(R.drawable.ic_user_avatar).centerCrop().fitCenter().into(holder.mProfile);
            holder.mFrom.setText(item.getFrom());
            holder.mTo.setText(item.getTo());

            try {
                String time = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(item.getTime()));
                holder.mLeavingTime.setText(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.mPhone.setText(item.getPhone());
            holder.mName.setText(item.getName());
            holder.btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    accept(item, position);
                }
            });
            holder.btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new AlertDialog.Builder(mContext)
                            .setTitle("Cancel")
                            .setMessage("Do you really want to cancel request?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    reject(item, position);
                                }})
                            .setNegativeButton(android.R.string.no, null).show();

                }
            });
        }
    }

    public void setCallback(CB cb){
        this.cb = cb;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView mProfile;
        TextView mName, mPhone, mLeavingTime, mTo, mFrom;
        View btnAccept;
        View btnCancel;

        public MyViewHolder(View itemView) {
            super(itemView);
            mProfile = (ImageView)itemView.findViewById(R.id.user_img);
            mName = (TextView)itemView.findViewById(R.id.user_name);
            mPhone = (TextView)itemView.findViewById(R.id.phone);
            mLeavingTime = (TextView)itemView.findViewById(R.id.time);
            mTo = (TextView)itemView.findViewById(R.id.to);
            mFrom = (TextView)itemView.findViewById(R.id.from);
            btnAccept = itemView.findViewById(R.id.accept);
            btnCancel = itemView.findViewById(R.id.cancel);
        }
    }

    private void accept(Interested obj, final int position){
        final ProgressDialogFragment mProgress = new ProgressDialogFragment();
        mProgress.setTextMessage("Please wait..");
        mProgress.show(mContext.getSupportFragmentManager(), "");
        HashMap<String, String> params = new HashMap<>();
        params.put("id", obj.getBookingId());
        params.put("userid", obj.getId());
        params.put("ownerid", mHelper.getUserId());
        params.put("type", mIntent.getString("type"));

        HTTP.makeHttpPostRequest(mContext, Utils.ACCEPT_REQUEST, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    try{
                        mList.remove(position);
                        notifyItemRemoved(position);
                    }catch (Exception ex){

                    }
                    //cb.onClick(REFRESH);
                }else {
                    Toast.makeText(mContext, "Error Occurred", Toast.LENGTH_SHORT).show();
                }
                mProgress.dismiss();
                Log.d("AcceptRequest", response);
            }

            @Override
            public void onError(String error) {
                mProgress.dismiss();
                Toast.makeText(mContext, "Networking Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void reject(Interested obj, final int position){
        HashMap<String, String> params = new HashMap<>();
        params.put("id", obj.getBookingId());
        mList.remove(position);
        notifyItemRemoved(position);
        HTTP.makeHttpPostRequest(mContext, Utils.CANCEL_BOOKING, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    mList.remove(position);
                    notifyItemRemoved(position);
                    Toast.makeText(mContext, "Request rejected", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(mContext, "Error Occurred", Toast.LENGTH_SHORT).show();
                }
                Log.d("AcceptRequest", response);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(mContext, "Networking Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public interface CB{
        void onClick(int action);
    }
}
