package com.novelsol.hamrahi.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Interested;
import com.novelsol.hamrahi.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ccn on 19/10/2017.
 */

public class AdapterSentRequests extends RecyclerView.Adapter<AdapterSentRequests.MyViewHolder> {

    ArrayList<Interested> mList;
    Context mContext;
    PreferencesHelper mHelper;

    public AdapterSentRequests(ArrayList<Interested> mList, Context mContext){
        this.mList = mList;
        this.mContext = mContext;
        mHelper = new PreferencesHelper(mContext);
    }


    @Override
    public AdapterSentRequests.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request_sent, parent, false);
        return new AdapterSentRequests.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
            final Interested item = mList.get(position);
            Glide.with(mContext).load(item.getPicture()).error(R.drawable.ic_user_avatar).centerCrop().fitCenter().into(holder.mProfile);
            holder.mFrom.setText(item.getFrom());
            holder.mTo.setText(item.getTo());
            try {
                String time = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(item.getTime()));
                holder.mLeavingTime.setText(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.mPhone.setText(item.getPhone());
            holder.mName.setText(item.getName());
        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Cancel")
                        .setMessage("Do you really want to cancel request?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                cancel(item, position);
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView mProfile;
        TextView mName, mPhone, mLeavingTime, mTo, mFrom;
        View btnCancel;

        public MyViewHolder(View itemView) {
            super(itemView);
            mProfile = (ImageView)itemView.findViewById(R.id.user_img);
            mName = (TextView)itemView.findViewById(R.id.user_name);
            mPhone = (TextView)itemView.findViewById(R.id.phone);
            mLeavingTime = (TextView)itemView.findViewById(R.id.time);
            mTo = (TextView)itemView.findViewById(R.id.to);
            mFrom = (TextView)itemView.findViewById(R.id.from);
            btnCancel = itemView.findViewById(R.id.cancel);
        }
    }

    private void cancel(Interested obj, final int position) {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", obj.getBookingId());

        HTTP.makeHttpPostRequest(mContext, Utils.CANCEL_BOOKING, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if (response.equalsIgnoreCase("success")) {
                    mList.remove(position);
                    notifyItemRemoved(position);
                    Toast.makeText(mContext, "Request cancelled", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Error Occurred", Toast.LENGTH_SHORT).show();
                }
                Log.d("AcceptRequest", response);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(mContext, "Networking Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
