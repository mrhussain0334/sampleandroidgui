package com.novelsol.hamrahi.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Interested;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ccn on 10/09/2017.
 */

public class ConfirmationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Interested> mList;
    FragmentActivity mContext;
    PreferencesHelper mHelper;
    CB cb;
    ProgressDialogFragment mProgress;

    public ConfirmationAdapter(ArrayList<Interested> mList, FragmentActivity mContext) {
        this.mList = mList;
        this.mContext = mContext;
        mHelper = new PreferencesHelper(mContext);
        mProgress = new ProgressDialogFragment();

    }

    public void setCallback(CB cb){
        this.cb = cb;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_confirmation, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, final int position) {
        if(holder1 instanceof MyViewHolder){
            MyViewHolder holder = (MyViewHolder)holder1;
            final Interested item = mList.get(position);
            holder.mName.setText(item.getName());
            holder.mPhone.setText(item.getPhone());
            holder.mTo.setText(item.getTo());
            holder.mFrom.setText(item.getFrom());

            try {
                String time = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(item.getTime()));
                holder.mTime.setText(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Glide.with(mContext).load(item.getPicture()).error(R.drawable.ic_user_avatar).centerCrop().fitCenter().into(holder.mProfile);

            holder.mConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callNetwork(view.getContext(), Utils.CONFIRM, item, item.getBookingId(), item.getType(), position);
                }
            });
            holder.mCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(mContext)
                            .setTitle("Cancel")
                            .setMessage("Do you really want to cancel request?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    cancel(item);

                                }})
                            .setNegativeButton(android.R.string.no, null).show();
                }
            });

        }
    }

    private void cancel(Interested obj){
        HashMap<String, String> params = new HashMap<>();
        params.put("id", obj.getBookingId());

        HTTP.makeHttpPostRequest(mContext, Utils.CANCEL_BOOKING, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    Toast.makeText(mContext, "Ride cancelled", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(mContext, "Error Occurred", Toast.LENGTH_SHORT).show();
                }
                Log.d("AcceptRequest", response);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(mContext, "Networking Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView mName, mTime, mTo, mFrom, mPhone;
        ImageView mProfile;
        View mConfirm, mCancel;

        public MyViewHolder(View itemView) {
            super(itemView);
            mName = (TextView)itemView.findViewById(R.id.user_name);
            mTime = (TextView)itemView.findViewById(R.id.time);
            mTo = (TextView)itemView.findViewById(R.id.to);
            mFrom = (TextView)itemView.findViewById(R.id.from);
            mPhone = (TextView)itemView.findViewById(R.id.phone);

            mProfile = (ImageView)itemView.findViewById(R.id.user_img);
            mConfirm = itemView.findViewById(R.id.confirm);
            mCancel = itemView.findViewById(R.id.cancel);
        }
    }

    public void callNetwork(final Context context, String url, Interested notification, String postId, String type, final int position){
        mProgress.setTextMessage("Please wait..");
        HashMap<String, String> params = new HashMap<>();

        params.put("id", notification.getBookingId());
        params.put("userid", mHelper.getUserId());
        params.put("ownerid", notification.getId());
        params.put("postid", postId);
        Log.v("POSTID",postId);
        params.put("type",type );
        mProgress.show(((FragmentActivity)context).getSupportFragmentManager(), "P");
        HTTP.makeHttpPostRequest(context, url, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    //cb.onClick(1);
                    mList.remove(position);
                    notifyItemRemoved(position);
                    Toast.makeText(context, "Ride has been accepted", Toast.LENGTH_LONG).show();
                }else{
                    Log.d("Notification", response);
                }
                mProgress.dismiss();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, "Networking error", Toast.LENGTH_LONG).show();
            }
        });

    }

    public interface CB{
        void onClick(int action);
    }
}
