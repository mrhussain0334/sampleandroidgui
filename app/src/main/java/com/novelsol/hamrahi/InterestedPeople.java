package com.novelsol.hamrahi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.novelsol.hamrahi.Adapters.InterestedPeopleAdapter;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Interested;
import com.novelsol.hamrahi.ui.MyTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class InterestedPeople extends AppCompatActivity {

    String bookingId;
    String type;
    String userId;

    PreferencesHelper mPreferences;
    RecyclerView mRecyclerView;
    InterestedPeopleAdapter mAdapter;
    ArrayList<Interested> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interested_people);
        mPreferences = new PreferencesHelper(this);
        userId = mPreferences.getUserId();
        Intent intent = getIntent();
        type = intent.getStringExtra("type");
        bookingId = intent.getStringExtra("post_id");
        //mAdapter = new InterestedPeopleAdapter(this, mList, intent);
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(mAdapter);
        callNetwork();
        setTitle("Interested People");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void callNetwork(){
        HashMap<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("postid", bookingId);
        HTTP.makeHttpPostRequest(this, Utils.INTERESTED_PEOPLE, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                JSONArray array;
                try{
                    array = new JSONArray(response);
                    for(int i=0; i<array.length(); i++){
                        JSONObject obj = array.getJSONObject(i);
                        Interested interested = new Interested(obj.getString("Userid"), obj.getString("Name"), obj.getString("Phone"), obj.getString("ProfilePic"), obj.getString("Status"));
                        interested.setBookingId(obj.getString("Bookingid"));
                        mList.add(interested);
                    }
                    Log.d("AvailableRides", "Total available Rides : "+mList.size());
                    if(mList.size() == 0){
                        findViewById(R.id.empty).setVisibility(View.VISIBLE);
                        ((MyTextView)findViewById(R.id.empty_txt)).setText("No results");
                    }else{
                        mAdapter.notifyDataSetChanged();
                    }
                }catch (JSONException ex){
                    Toast.makeText(InterestedPeople.this, response, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(InterestedPeople.this, "Networking error", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
