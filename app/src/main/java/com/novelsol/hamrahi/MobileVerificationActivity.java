package com.novelsol.hamrahi;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.tapadoo.alerter.Alerter;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;


/**
 * Created by Shahzaib on 7/16/2017.
 */

public class MobileVerificationActivity extends AppCompatActivity {

    static EditText etVerification[] = new EditText[4];
    String mCode;
    String mPhone;
    PreferencesHelper mHelper;
    Button mSubmitCode, mResend;

    String SENT = "SMS_SENT";
    String DELIVERED = "SMS_DELIVERED";

    PreferencesHelper mPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mobile_verification);
        mHelper = new PreferencesHelper(this);
        mSubmitCode = (Button) findViewById(R.id.submit_code);
        mResend = (Button) findViewById(R.id.resend);
        etVerification[0] = (EditText) findViewById(R.id.c_1);
        etVerification[1] = (EditText) findViewById(R.id.c_2);
        etVerification[2] = (EditText) findViewById(R.id.c_3);
        etVerification[3] = (EditText) findViewById(R.id.c_4);

        mPreferences = new PreferencesHelper(this);
        Intent intent = getIntent();

        mPhone = intent.getStringExtra("phone");
        mCode = getCode();
        //sendCode();
        mResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCode = getCode();
                //sendCode();
                sendCodeToServer();
            }
        });

        mSubmitCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder codeBuilder = new StringBuilder();
                codeBuilder.append(etVerification[0].getText().toString());
                codeBuilder.append(etVerification[1].getText().toString());
                codeBuilder.append(etVerification[2].getText().toString());
                codeBuilder.append(etVerification[3].getText().toString());

                if (mCode.equals(codeBuilder.toString())) {
                    verifyOnServer();
                }else{
                    Alerter.create(MobileVerificationActivity.this)
                            .setTitle("Error")
                            .setText("Code is not valid. Try with a new code.")
                            .setBackgroundColorRes(R.color.colorAccent)
                            .enableSwipeToDismiss()
                            .show();
                }

            }
        });
        mResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCodeToServer();
            }
        });

        etVerification[0].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    etVerification[1].requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etVerification[1].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    etVerification[2].requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etVerification[2].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    etVerification[3].requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        sendCodeToServer();
    }



    void putCode(String code){
        etVerification[0].setText(""+code.charAt(0));
        etVerification[1].setText(""+code.charAt(1));
        etVerification[2].setText(""+code.charAt(2));
        etVerification[3].setText(""+code.charAt(3));
    }

    private void registerBroadCastReceivers(){

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {

                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {

                    SmsMessage[] messages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                    String number = messages[0].getOriginatingAddress();
                    StringBuilder sb = new StringBuilder();
                    for (SmsMessage message : messages) {
                        sb.append(message.getDisplayMessageBody());
                    }

                    String code = sb.toString().replaceAll("[^0-9]", "");

                    if(code.equals(mCode)){
                        mHelper.setCodeReceived(true);
                        putCode(code);
                    }

                }
            }
        }, intentFilter);

    }

    private String getCode(){
        Random random = new Random();
        return String.format("%04d",random.nextInt(10000));
    }

    private void sendCode() {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            String verificationCode = getCode();
            mCode = verificationCode;
            PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
            PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
            registerBroadCastReceivers();
            smsManager.sendTextMessage(mPhone, null, verificationCode+" is your verification code for Ride Sharing.", sentPI, deliveredPI);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void sendCodeToServer(){
        HashMap<String, String> params = new HashMap<>();
        params.put("email", mPreferences.getEmail());
        params.put("code", mCode);
        HTTP.makeHttpPostRequest(this, Utils.POST_VERIFY_KEY, params, new HTTP.RequestCompleteCallback(){

            @Override
            public void onRequestComplete(String response) {
                Log.v("Verify", response);
                if(response.equalsIgnoreCase("success")){
                    Toast.makeText(getApplication(), "Code sent", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(MobileVerificationActivity.this, "Networking Error ",Toast.LENGTH_SHORT).show();
            }
        });
    }

    void verifyOnServer(){
        HashMap<String, String> params = new HashMap<>();
        params.put("email", mPreferences.getEmail());
        params.put("code", mCode);
        HTTP.makeHttpPostRequest(MobileVerificationActivity.this, Utils.VERIFY_PHONE, params, new HTTP.RequestCompleteCallback(){

            @Override
            public void onRequestComplete(String response) {
                Log.v("Verify", response);
                if(response.equalsIgnoreCase("success")){
                    Toast.makeText(MobileVerificationActivity.this, "User verified", Toast.LENGTH_SHORT).show();
                    mHelper.setLogin(true);
                    startActivity(new Intent(MobileVerificationActivity.this, MainActivity.class));
                    finish();
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(MobileVerificationActivity.this, "Networking Error ",Toast.LENGTH_SHORT).show();
            }
        });
    }
}

