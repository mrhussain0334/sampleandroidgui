package com.novelsol.hamrahi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.ui.MyEditText;
import com.novelsol.hamrahi.ui.MyTextView;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;
import com.tapadoo.alerter.Alerter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    boolean f1, f2, f3, f4, f5;

    MyEditText mName, mEmail, mPhone, mPassword, mRPassword;
    MyTextView mSignUp;
    PreferencesHelper mHelper;
    ProgressDialogFragment mProgress;
    String phoneNumber = null;
    View linkLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mName = (MyEditText)findViewById(R.id.name);
        mEmail = (MyEditText)findViewById(R.id.email);
        mPhone = (MyEditText)findViewById(R.id.phone);
        mPassword = (MyEditText)findViewById(R.id.password);
        mSignUp = (MyTextView)findViewById(R.id.signup);
        mRPassword = (MyEditText)findViewById(R.id.r_password);
        mProgress = new ProgressDialogFragment();
        mProgress.setTextMessage("Creating account..");
        mSignUp.setOnClickListener(this);
        mHelper = new PreferencesHelper(this);
        linkLogin = findViewById(R.id.link_login);
        linkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                RegisterActivity.this.finish();
            }
        });
    }


    @Override
    public void onClick(View v) {

        if(mName.length()>0){
            f1 = true;
        }else{
            mName.setError("Name cannot be empty");
            f1 = false;
        }

        if(mPhone.getText().toString().length()>=10){
            f3 = true;
        }else{
            mPhone.setError("Please enter a phone number");
            f3 = false;
        }

        if(isValidEmail(mEmail.getText().toString())){
            f2 = true;

        }else{
            mEmail.setError("Email not valid");
            f2 = false;
        }

        if(isValidPassword(mPassword.getText().toString())){
            f4 = true;
        }else {
            Alerter.create(this)
                    .setTitle("Error")
                    .setText("Password must contain at least one Uppercase, one lowercase, one Special Character and a Number")
                    .setBackgroundColorRes(R.color.colorAccent)
                    .enableSwipeToDismiss()
                    .show();
            mPassword.setError("Password not valid");
            f4 = false;
        }
        if((mRPassword.getText().toString().length()==mPassword.getText().toString().length())&&mPassword.getText().toString().equals(mRPassword.getText().toString())){
            f5 = true;
        }else{
            f5 = false;
            mPassword.setError("Passwords does not match");
            mRPassword.setError("Passwords does not match");
        }

        if(f1&&f2&&f3&&f4&&f5){
            signup();
        }
    }

    void signup(){
        HashMap<String, String> params = new HashMap<>();

        params.put("name", mName.getText().toString());
        params.put("phone", mPhone.getText().toString());
        params.put("email", mEmail.getText().toString());
        params.put("password", mPassword.getText().toString());

        mProgress.show(getSupportFragmentManager(),"Dialog");

        HTTP.makeHttpPostRequest(this, Utils.SIGNUP, params,  new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                JSONObject res;
                mProgress.dismiss();
                try{
                    res = new JSONObject(response);
                    mHelper.setUserId(res.getString("Userid"));
                    mHelper.setUserName(res.getString("Name"));
                    mHelper.setUserPhone(phoneNumber);
                    mHelper.setEmail(res.getString("Email"));
                    mHelper.setPassword(res.getString("Password"));
                    mHelper.setUserImg(res.getString("ProfilePic"));
                    mHelper.setLogin(true);

                    String verified = res.getString("VerifyStatus");

                    if(verified.equalsIgnoreCase("N")){
                        mHelper.setLogin(false);
                        Intent intent = new Intent(RegisterActivity.this, MobileVerificationActivity.class);
                        intent.putExtra("phone", res.getString("Phone"));
                        startActivity(intent);
                    }else{
                        mHelper.setLogin(true);
                        startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                    }
                    finish();

                }catch (JSONException ex){
                    mProgress.dismiss();
                    ex.printStackTrace();
                    Toast.makeText(RegisterActivity.this, response, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                mProgress.dismiss();
                Toast.makeText(RegisterActivity.this, "Networking error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public boolean isValidEmail(String email){
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public boolean isValidPhone(String phone){
        String PHONE_PATTERN = "^[+]?[0-9]{10,13}$";
        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        Matcher matcher = pattern.matcher(phone);

        return matcher.matches();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }

    public boolean isValidPassword(String password){
        String PHONE_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=*!])(?=\\S+$).{8,}$";
        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        Matcher matcher = pattern.matcher(password);

        return matcher.matches();
    }



}
