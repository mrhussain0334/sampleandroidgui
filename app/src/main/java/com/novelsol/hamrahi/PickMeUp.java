package com.novelsol.hamrahi;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.novelsol.hamrahi.Adapters.PlaceAutocompleteAdapter;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.LocationHelper;
import com.novelsol.hamrahi.Helper.PlaceJSONParser;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.ui.CustomAutoCompleteEditText;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static com.novelsol.hamrahi.R.id.map;

public class PickMeUp extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, PlaceAutocompleteAdapter.PlaceSelectionListener, RoutingListener, TimePickerDialog.OnTimeSetListener,HTTP.RequestCompleteCallback {

    Toolbar mToolbar;
    private GoogleMap mMap;
    CustomAutoCompleteEditText mPickFrom;
    CustomAutoCompleteEditText mPickTo;
    PlaceAutocompleteAdapter mPlaceAutoCompleteAdapter;

    private static final String TAG = "PickMeUp";
    Button mPickMe;

    ListView mLocationsListView;
    RelativeLayout mEmptyViewContainer;

    double longitude = 0.0;
    double latitude = 0.0;

    LinearLayout mLocationsContainer;
    GoogleApiClient mGoogleApiClient;
    private static final LatLngBounds BOUNDS = new LatLngBounds(
            new LatLng(31.401422, 74.211078), new LatLng(31.554606, 74.357158));

    LatLng mStart, mEnd;
    private static final int[] COLORS = new int[]{R.color.colorPrimaryDark,R.color.colorPrimary, R.color.colorAccent};

    boolean isLocationSelected = false;
    boolean isTimeSelected = false;

    TimePickerDialog timePicker;
    Button mPickTime;
    EditText mEtHrs, mEtMins, mEtMedium;

    PreferencesHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_me_up);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mPickFrom = (CustomAutoCompleteEditText)findViewById(R.id.pick_from);
        mPickTo = (CustomAutoCompleteEditText)findViewById(R.id.pick_to);
        mPickTo.requestFocus();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync(this);
        mHelper = new PreferencesHelper(this);
        mPickMe = (Button)findViewById(R.id.pick_me);
        mPickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSuccess();
            }
        });
        mPickTime = (Button)findViewById(R.id.pick_time);

        mEtHrs = (EditText)findViewById(R.id.hrs);
        mEtMins = (EditText)findViewById(R.id.mins);
        mEtMedium = (EditText)findViewById(R.id.period);
        timePicker = TimePickerDialog.newInstance(this, true);
        mPickTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePicker.show(getFragmentManager(), "TimePicker");
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        if(timePicker != null) timePicker.setOnTimeSetListener(this);


        mPlaceAutoCompleteAdapter = new PlaceAutocompleteAdapter(getApplicationContext(), mGoogleApiClient, BOUNDS, null);

        mLocationsContainer = (LinearLayout)findViewById(R.id.locations_container);
        Animation slideUpAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_up_locations);
        mLocationsContainer.startAnimation(slideUpAnimation);

        mLocationsListView = (ListView)findViewById(R.id.list_view);
        mEmptyViewContainer = (RelativeLayout)findViewById(R.id.loc_empty_view);

        mPickTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {

                if(mLocationsContainer.getVisibility() == INVISIBLE){
                    Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up_locations);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {}

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            mLocationsContainer.setVisibility(View.VISIBLE);
                            mPlaceAutoCompleteAdapter.getFilter().filter(s);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {}
                    });
                    mLocationsContainer.startAnimation(anim);
                }else{
                    mPlaceAutoCompleteAdapter.getFilter().filter(s);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        mPlaceAutoCompleteAdapter.setQueryCompleteCallback(new PlaceAutocompleteAdapter.QueryCompleteCallback() {
            @Override
            public void onQueryComplete(int numResults) {
                if(numResults>0){
                    if(mEmptyViewContainer.getVisibility() == View.VISIBLE){
                        mEmptyViewContainer.setVisibility(View.INVISIBLE);
                    }
                }else if(numResults==0){
                    if(mEmptyViewContainer.getVisibility() == View.INVISIBLE){
                        mEmptyViewContainer.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        mPlaceAutoCompleteAdapter.setOnPlaceSelectionListener(this);
        mLocationsListView.setAdapter(mPlaceAutoCompleteAdapter);

        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    public boolean onNavigateUp() {
        finish();
        return super.onNavigateUp();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {}
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        MapsInitializer.initialize(this);
        Location curLocation = LocationHelper.getLastKnownLocation(this);
        if(curLocation!=null){
            latitude = curLocation.getLatitude();
            longitude = curLocation.getLongitude();
            if(mMap!=null){
                LatLng loc = new LatLng(latitude, longitude);
                mStart = loc;
                mPickFrom.setText(getLocationNameByLatLng(loc));
            }
        }
    }

    ProgressDialogFragment dialog;

    private void showSuccess(){

        Calendar c = Calendar.getInstance();

        SimpleDateFormat S = new SimpleDateFormat("yyyy-MM-dd");

        String date = S.format(c.getTime());

        if(!isLocationSelected){
            Toast.makeText(PickMeUp.this, "Please select location first", Toast.LENGTH_SHORT).show();
        }else if(!isTimeSelected){
            Toast.makeText(PickMeUp.this, "Please select time", Toast.LENGTH_SHORT).show();
        }else{
            Location location = LocationHelper.getLastKnownLocation(this);
            HashMap<String, String> params = new HashMap<>();
            params.put("userid", mHelper.getUserId());
            params.put("startfrom", mPickFrom.getText().toString());
            params.put("destination", mPickTo.getText().toString());

            String strDate = date+" "+mEtHrs.getText().toString()+":"+mEtMins.getText().toString()+":00";
            params.put("leavingtime", strDate);
            params.put("seat", "4");
            params.put("longstartfrom", String.valueOf(location.getLongitude()));
            params.put("latstartfrom", String.valueOf(location.getLatitude()));
            params.put("longdestination", String.valueOf(mEnd.longitude));
            params.put("latdestination", String.valueOf(mEnd.latitude));

            Log.v("Pick", "Leaving time : "+ strDate);

            HTTP.makeHttpPostRequest(PickMeUp.this, Utils.POST_RIDER_REQUEST,params, this);
            dialog = new ProgressDialogFragment();
            dialog.setTextMessage("Posting..");
            dialog.show(getSupportFragmentManager(), "P");
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onPlaceSelected(AutocompletePrediction place) {

        Places.GeoDataApi.getPlaceById(mGoogleApiClient, place.getPlaceId())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {

                        Animation anim = AnimationUtils.loadAnimation(PickMeUp.this, R.anim.slide_down_locations);
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {}

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                mLocationsContainer.setVisibility(INVISIBLE);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {}
                        });

                        mLocationsContainer.startAnimation(anim);
                        if (places.getStatus().isSuccess()) {
                            final Place myPlace = places.get(0);
                            LatLng loc = myPlace.getLatLng();
                            mEnd = loc;
                            mPickTo.setText(myPlace.getAddress());
                            mPickTo.setFocusable(false);
                            getRoute();
                        }
                        places.release();
                    }
                });
    }

    ProgressDialog progressDialog;

    void getRoute(){
        progressDialog = ProgressDialog.show(PickMeUp.this, "Please wait.",
                "Fetching route information.", true);
        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .waypoints(mStart, mEnd)
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(), "Error while getting route", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRoutingStart() {

    }

    static ArrayList<Polyline> polylines = new ArrayList<>();

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int j) {
        progressDialog.dismiss();
        CameraUpdate center = CameraUpdateFactory.newLatLng(mStart);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);

        mMap.moveCamera(center);
        mMap.moveCamera(zoom);
        mMap.clear();

        mMap.addMarker(new MarkerOptions()
                .position(mStart)
                .title("Your location"));

        mMap.addMarker(new MarkerOptions()
                .position(mEnd)
                .title("Your destination"));

        polylines = new ArrayList<>();

        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();

        for (int i = 0; i < route.size(); i++) {

            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(ResourcesCompat.getColor(getResources(), COLORS[colorIndex], null));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);
        }

        isLocationSelected = true;

    }

    String getLocationNameByLatLng(LatLng mLatLng){
        List<Address> addresses = null;

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());


        try {
            addresses = geocoder.getFromLocation(
                    mLatLng.latitude,
                    mLatLng.longitude,
                    5);
        } catch (IOException ioException) {

        } catch (IllegalArgumentException illegalArgumentException) {

        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size()  == 0) {
            return null;
            //deliverResultToReceiver(Constants.FAILURE_RESULT, errorMessage);
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<String>();

            for(int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }
            return address.getAddressLine(0);
        }
    }

    @Override
    public void onRoutingCancelled() {
        progressDialog.dismiss();
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        mEtHrs.setText(hourOfDay+"");
        mEtMins.setText(minute+"");
        mEtMedium.setText(hourOfDay>12?"PM":"AM");
        isTimeSelected = true;
    }

    @Override
    public void onRequestComplete(String response) {
        if(response.equalsIgnoreCase("success")){
            Toast.makeText(getApplicationContext(), "Request has been Posted", Toast.LENGTH_SHORT).show();
            PickMeUp.this.finish();
        }else{
            Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(String error) {
        Toast.makeText(getApplication(), "Network error", Toast.LENGTH_SHORT).show();
    }
}
