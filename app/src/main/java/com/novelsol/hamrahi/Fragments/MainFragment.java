package com.novelsol.hamrahi.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.novelsol.hamrahi.AvailableRideActivity;
import com.novelsol.hamrahi.GetSomeone;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.PickMeUp;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.RideRequestActivity;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {


    ImageView info1a, info2a, info3a, info1b, info2b, info3b, info4a, info4b;
    RelativeLayout mCard1, mCard2, mCard3, mCard4;

    boolean c1Clicked = false, c2Clicked = false, c3Clicked = false, c4Clicked = false;

    public static final int ONE = 1, TWO = 2, THREE = 3, FOUR = 4;

    PreferencesHelper mHelper;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_main, container, false);


        mHelper = new PreferencesHelper(getContext());
       // if(mHelper.getFCMToken().length()==0){
            mHelper.setFCMTOken(FirebaseInstanceId.getInstance().getToken());
            Log.d("FCM", mHelper.getFCMToken());
            sendRegistrationToServer(mHelper.getFCMToken());
      //  }else{
      //      Log.d("FCM", mHelper.getFCMToken());
      //  }

        mCard1 = (RelativeLayout) view.findViewById(R.id.card1_a);
        mCard2 = (RelativeLayout) view.findViewById(R.id.card2_a);
        mCard3 = (RelativeLayout) view.findViewById(R.id.card3_a);
        mCard4 = (RelativeLayout) view.findViewById(R.id.card4_a);

        info1a = (ImageView)view.findViewById(R.id.info_1);
        info2a = (ImageView)view.findViewById(R.id.info_2);
        info3a = (ImageView)view.findViewById(R.id.info_3);
        info4a = (ImageView)view.findViewById(R.id.info_4);

        info1b = (ImageView)view.findViewById(R.id.c_info_1);
        info2b = (ImageView)view.findViewById(R.id.c_info_2);
        info3b = (ImageView)view.findViewById(R.id.c_info_3);
        info4b = (ImageView)view.findViewById(R.id.c_info_4);


        View.OnClickListener cardListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.info_1:{
                        expandCard(ONE);
                        break;
                    }case R.id.info_2:{
                        expandCard(TWO);
                        break;
                    }case R.id.info_3:{
                        expandCard(THREE);
                        break;
                    }case R.id.info_4:{
                        expandCard(FOUR);
                        break;
                    }case R.id.c_info_1:{
                        collapseCard(ONE);
                        break;
                    }case R.id.c_info_2:{
                        collapseCard(TWO);
                        break;
                    }case R.id.c_info_3:{
                        collapseCard(THREE);
                        break;
                    }case R.id.c_info_4:{
                        collapseCard(FOUR);
                        break;
                    }case R.id.card1_a:{
                        startActivity(new Intent(getContext(), PickMeUp.class));
                        break;
                    }case R.id.card2_a:{
                        startActivity(new Intent(getContext(), GetSomeone.class));
                        break;
                    }case R.id.card3_a:{
                        startActivity(new Intent(getContext(), AvailableRideActivity.class));
                        break;
                    }case R.id.card4_a:{
                        startActivity(new Intent(getContext(), RideRequestActivity.class));
                        break;
                    }
                }
            }
        };

        info1a.setOnClickListener(cardListener);
        info2a.setOnClickListener(cardListener);
        info3a.setOnClickListener(cardListener);
        info4a.setOnClickListener(cardListener);


        info1b.setOnClickListener(cardListener);
        info2b.setOnClickListener(cardListener);
        info3b.setOnClickListener(cardListener);
        info4b.setOnClickListener(cardListener);

        mCard1.setOnClickListener(cardListener);
        mCard2.setOnClickListener(cardListener);
        mCard3.setOnClickListener(cardListener);
        mCard4.setOnClickListener(cardListener);


        return view;
    }

    private void sendRegistrationToServer(String token) {
        HashMap<String, String> params = new HashMap<>();
        params.put("email", mHelper.getEmail());
        params.put("fcmid",token);

        HTTP.makeHttpPostRequest(getContext(), Utils.POST_FCM, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                Log.d("MainActivity", "Token Posted Response : " + response);
            }

            @Override
            public void onError(String error) {
                Log.d("MainActivity", "Network error");
            }
        });
    }

    public void expandCard(final int i){
        Animation animation = AnimationUtils.loadAnimation(getContext(),
                R.anim.slide_up);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                switch (i){
                    case ONE:{
                        mCard1.setVisibility(View.INVISIBLE);
                        c1Clicked = !c1Clicked;
                        break;
                    }case TWO:{
                        mCard2.setVisibility(View.INVISIBLE);
                        c2Clicked = !c2Clicked;
                        break;
                    }case THREE:{
                        mCard3.setVisibility(View.INVISIBLE);
                        c3Clicked = !c3Clicked;
                        break;
                    }case FOUR:{
                        mCard4.setVisibility(View.INVISIBLE);
                        c4Clicked = !c4Clicked;
                        break;
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        switch (i){
            case ONE:{
                mCard1.startAnimation(animation);
                break;
            }case TWO:{
                mCard2.startAnimation(animation);
                break;
            }case THREE:{
                mCard3.startAnimation(animation);
                break;
            }case FOUR:{
                mCard4.startAnimation(animation);
                break;
            }
        }
    }


    public void collapseCard(final int i){
        Animation animation = AnimationUtils.loadAnimation(getContext(),
                R.anim.slide_down);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                switch (i){
                    case ONE:{
                        mCard1.setVisibility(View.VISIBLE);
                        c1Clicked = !c1Clicked;
                        break;
                    }case TWO:{
                        mCard2.setVisibility(View.VISIBLE);
                        c2Clicked = !c2Clicked;
                        break;
                    }case THREE:{
                        mCard3.setVisibility(View.VISIBLE);
                        c3Clicked = !c3Clicked;
                        break;
                    }case FOUR:{
                        mCard4.setVisibility(View.VISIBLE);
                        c4Clicked = !c4Clicked;
                        break;
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        switch (i){
            case ONE:{
                mCard1.startAnimation(animation);
                break;
            }case TWO:{
                mCard2.startAnimation(animation);
                break;
            }case THREE:{
                mCard3.startAnimation(animation);
                break;
            }case FOUR:{
                mCard4.startAnimation(animation);
                break;
            }
        }
    }

}
