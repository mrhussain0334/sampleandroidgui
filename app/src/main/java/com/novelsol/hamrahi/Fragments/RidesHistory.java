package com.novelsol.hamrahi.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.novelsol.hamrahi.Adapters.RideHistoryAdapter;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.History;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.MyTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ccn on 27/07/2017.
 */

public class RidesHistory extends Fragment {
    RecyclerView mRecyclerView;
    RideHistoryAdapter mAdapter;
    ArrayList<History> mList = new ArrayList<>();

    private static final int LOGIN_AS_DRIVER = 0;
    private static final int LOIGN_AS_RIDER = 1;
    PreferencesHelper mHelper;
    SwipeRefreshLayout mSwipe;

    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rides_history, container, false);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.history_recycler_view);
        mSwipe = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayout);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callItem();
            }
        });
        mAdapter = new RideHistoryAdapter(mList);
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(mAdapter);
        mHelper = new PreferencesHelper(getContext());
        callItem();
        this.view = view;
        return view;
    }

    private void callItem() {
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", mHelper.getUserId());
        params.put("type", mHelper.getLoginAs()==LOGIN_AS_DRIVER?"d":"r");
        HTTP.makeHttpPostRequest(getContext(),Utils.HISTORY, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                Log.v("HISTORY", response);
                mList.clear();
                try {
                    JSONArray array = new JSONArray(response);
                    for(int i=0; i<array.length(); i++){
                        JSONObject jsonObject = array.getJSONObject(i);
                        History obj = new History(jsonObject.getString("StartFrom"),jsonObject.getString("Destination"),jsonObject.getString("LeavingTime"),jsonObject.getString("Name"), jsonObject.getString("ProfilePic"));
                        obj.setDriverName(jsonObject.getString("Name"));
                        obj.setDriverPic(jsonObject.getString("ProfilePic"));
                        obj.setRating(jsonObject.getString("rating"));
                        mList.add(obj);
                    }
                    if(mList.size()==0){
                        view.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                        ((MyTextView)view.findViewById(R.id.empty_txt)).setText("No history");
                    }else{
                        if(view.findViewById(R.id.empty).getVisibility()==View.VISIBLE){
                            view.findViewById(R.id.empty).setVisibility(View.INVISIBLE);
                        }
                    }
                    if(mSwipe.isRefreshing()){
                        mSwipe.setRefreshing(false);
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    if(mSwipe.isRefreshing()){
                        mSwipe.setRefreshing(false);
                    }
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(String error) {

            }
        });
    }
}
