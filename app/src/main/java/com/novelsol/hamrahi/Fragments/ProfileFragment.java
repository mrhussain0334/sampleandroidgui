package com.novelsol.hamrahi.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.MainActivity;
import com.novelsol.hamrahi.ProfileActivity;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.EditProfileDialog;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements EditProfileDialog.OnDataSavedCallback {


    TextView mHeaderName;
    TextView mName;
    TextView mEmail;
    TextView mPhone;

    PreferencesHelper mHelper;

    EditProfileDialog mDialog;
    FloatingActionButton mFab;
    CircleImageView mProfilePic;

    private static final int REQUEST_CODE_PICKER = 300;
    TextView mRating;
    ProgressDialogFragment mProgress;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);

        mHelper = new PreferencesHelper(getContext());
        mHeaderName = (TextView)view.findViewById(R.id.header_name);
        mName = (TextView)view.findViewById(R.id.name);
        mEmail = (TextView)view.findViewById(R.id.email);
        mPhone = (TextView)view.findViewById(R.id.phone);
        mFab = (FloatingActionButton)view.findViewById(R.id.fab);
        mProfilePic = (CircleImageView)view. findViewById(R.id.photo);
        mRating = (TextView)view.findViewById(R.id.rating);
        mHeaderName.setText(mHelper.getUserName());
//        mName.setText(mHelper.getUserName());
        mEmail.setText(mHelper.getEmail());
        mProgress = new ProgressDialogFragment();
        mProgress.setTextMessage("Please wait..");
        //mProgress.show(getActivity().getSupportFragmentManager(), "P");
        mPhone.setText(mHelper.getUserPhone());
        Glide.with(this).load(mHelper.getUserImg()).fitCenter().centerCrop().placeholder(R.drawable.ic_user_avatar).error(R.drawable.ic_user_avatar).into(mProfilePic);
        getActivity().setTitle("Profile");
        view.findViewById(R.id.photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPic(v);
            }
        });
        mProgress = new ProgressDialogFragment();
        mProgress.show(getActivity().getSupportFragmentManager(),"P");

        view.findViewById(R.id.cp1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit(v);
            }
        });
        view.findViewById(R.id.cp2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit(v);
            }
        });
        view.findViewById(R.id.cp3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit(v);
            }
        });
        getReview();
        return view;
    }

    private void getReview(){
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", mHelper.getUserId());
        params.put("type", mHelper.getLoginAs()==0?Utils.TYPE_DRIVER:Utils.TYPE_RIDER);
        HTTP.makeHttpPostRequest(getContext(), Utils.GET_REVIEW, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {

                try{
                    JSONObject res = new JSONObject(response);
                    double rating = res.getDouble("Rating");
                    mRating.setText(rating+"");
                }catch (JSONException ex){
                    mRating.setText("0.0");
                }
                mProgress.dismiss();
            }

            @Override
            public void onError(String error) {

            }
        });

    }


    public void edit(View view){
        Bundle bundle = new Bundle();
        switch (view.getId()){
            case 1:{
                bundle.putString("title", "Enter name");
                mDialog = EditProfileDialog.getInstance(EditProfileDialog.EDIT_NAME);
                break;
            }
            case R.id.cp1:
            case R.id.cp2:
            case R.id.cp3:{
                bundle.putString("title", "Enter password");
                mDialog = EditProfileDialog.getInstance(EditProfileDialog.EDIT_PASSWORD);
                mDialog.setArguments(bundle);
                break;
            }
        }
        mDialog.setOnDataSavedCallback(this);
        mDialog.show(getChildFragmentManager(), "EditProfile");
    }

    @Override
    public void onSave(int type, String newData) {
        switch (type){
            case EditProfileDialog.EDIT_NAME:{
                mName.setText(newData);
                mHeaderName.setText(newData);
            }
        }
    }

    public void update(){
        HashMap<String, String> params = new HashMap<>();
        params.put("id", mHelper.getUserId());
        params.put("name", mHelper.getUserName());
        params.put("password", mHelper.getPassword());

        HTTP.makeHttpPostRequest(getContext(), Utils.UPDATE_PROFILE, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equals("Success")){
                    Toast.makeText(getContext(), "Profile updated", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getContext(), "Error while updating profile", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void selectPic(View view){
        ImagePicker.create(this)
                .returnAfterFirst(true) // set whether pick or camera action should return immediate result or not. For pick image only work on single mode
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .theme(R.style.AppTheme_NoActionBar) // must inherit ef_BaseTheme. please refer to sample
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            Image image = (ImagePicker.getImages(data)).get(0);
            try {
                postImage(new File(image.getPath()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    void postImage(File file) throws FileNotFoundException {

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("pic", file);
        params.put("id", mHelper.getUserId());

        client.post(getContext(), "http://ufmo.asia/RidesharingApi/public/user/pic/qweasdzxc123", params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                try {
                    String response = new String(responseBody,"UTF-8");
                    Glide.with(getContext())
                            .load(response)
                            .fitCenter()
                            .centerCrop()
                            .error(R.drawable.ic_user_avatar)
                            .into(mProfilePic);
                    Glide.with(getContext())
                            .load(response)
                            .fitCenter()
                            .centerCrop()
                            .error(R.drawable.ic_user_avatar)
                            .into(MainActivity.mProfile);
                    Log.d("Image", response);
                    mHelper.setUserImg(response);
                    Toast.makeText(getContext(), "Image updated", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    String response = new String(responseBody,"UTF-8");
                    Log.e("Error", response);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                Log.d("Uploading", "Progress : "+bytesWritten/totalSize+" %");
            }
        });

    }
}
