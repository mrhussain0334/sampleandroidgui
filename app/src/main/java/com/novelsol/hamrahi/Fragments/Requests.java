package com.novelsol.hamrahi.Fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.novelsol.hamrahi.Driver.DriverConfirmation;
import com.novelsol.hamrahi.Driver.DriverRequests;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Requests extends Fragment {

    TabLayout mTabs;
    ViewPager mViewPager;
    Adapter mViewPagerAdapter;
    PreferencesHelper mHelper;

    public Requests() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_requests2, container, false);
        mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        mTabs = (TabLayout)view.findViewById(R.id.tabs);
        mViewPagerAdapter = new Adapter(getChildFragmentManager());
        mHelper = new PreferencesHelper(getContext());

        Bundle bundle = new Bundle();

        String type = mHelper.getLoginAs() == 0?Utils.TYPE_DRIVER:Utils.TYPE_RIDER;

        bundle.putString("type", type);
        getActivity().setTitle("Requests");
        DriverRequests driverRequestsFragment = new DriverRequests();
        driverRequestsFragment.setArguments(bundle);
    //    DriverConfirmation driverConfirmationFragment = new DriverConfirmation();
        mViewPagerAdapter.addPage(driverRequestsFragment, "received");
        bundle.putString("type", type);
    //    driverConfirmationFragment.setArguments(bundle);
        SentRequests sentRequests = new SentRequests();
        mViewPagerAdapter.addPage(sentRequests, "sent");
        mViewPager.setAdapter(mViewPagerAdapter);
        mTabs.setupWithViewPager(mViewPager);
        return view;
    }


    class Adapter extends FragmentPagerAdapter{

        ArrayList<Fragment> mPages;
        ArrayList<String> mTitles;

        public Adapter(FragmentManager fm) {
            super(fm);
            mPages = new ArrayList<>();
            mTitles = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int position) {
            return mPages.get(position);
        }

        @Override
        public int getCount() {
            return mPages.size();
        }

        void addPage(Fragment fragment, String title){
            mPages.add(fragment);
            mTitles.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position);
        }
    }

}
