package com.novelsol.hamrahi.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.novelsol.hamrahi.Adapters.ScheduledAdapter;
import com.novelsol.hamrahi.Adapters.SectionHeader;
import com.novelsol.hamrahi.Driver.DriverDashboard;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Scheduled;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.Rider.RiderDashboard;
import com.novelsol.hamrahi.ui.MyTextView;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ccn on 27/07/2017.
 */

public class ScheduledRides extends Fragment {

    ArrayList<SectionHeader> mList = new ArrayList<>();
    RecyclerView mRecyclerView;
    ScheduledAdapter mAdapter;
    PreferencesHelper mHelper;
    ArrayList<Scheduled> mList1 = new ArrayList<>();
    ArrayList<Scheduled> mList2 = new ArrayList<>();
    View view;
    ProgressDialogFragment mProgress;

    SwipeRefreshLayout mSwipe;

    Button btn;
    private static final int LOGIN_AS_DRIVER = 0;
    private static final int LOIGN_AS_RIDER = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_scheduled, container, false);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerview);
        Bundle bundle = getArguments();
        final String type = bundle.getString("type");
        mSwipe = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayout);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callNetwork(type);
            }
        });

        mAdapter = new ScheduledAdapter(getContext(), mList, bundle);
        mProgress = new ProgressDialogFragment();
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(mAdapter);
        mHelper = new PreferencesHelper(getContext());
        callNetwork(type);
        btn = (Button)view.findViewById(R.id.post_ride);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = mHelper.getLoginAs()==LOGIN_AS_DRIVER?new DriverDashboard():new RiderDashboard();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null);
                transaction.replace(R.id.container, newFragment);
                transaction.commit();
            }
        });
        return view;
    }

    void callNetwork(final String type){
        HashMap<String, String> params = new HashMap<>();

        params.put("userid", mHelper.getUserId());
        params.put("type", type);
        mProgress.setTextMessage("Loading..");
        mProgress.show(getActivity().getSupportFragmentManager(), "P");

        HTTP.makeHttpPostRequest(getContext(), Utils.GET_SCHEDULED, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                JSONArray array;
                Log.v("RESPONSE", response);
                mProgress.dismiss();
                mList.clear();
                mList.add(new SectionHeader(mList1, "CONFIRMED"));
                mList.add(new SectionHeader(mList2, "UNCONFIRMED"));
                mList1.clear();
                mList2.clear();
                try{
                    array = new JSONArray(response);
                    for(int i=0; i< array.length(); i++){
                        JSONObject object = array.getJSONObject(i);
                        if(object.getString("Userid").equals(mHelper.getUserId())){
                            if(type.equalsIgnoreCase("r")){
                                Scheduled scheduled = new Scheduled(object.getString("Riderid"),object.getString("Userid"), object.getString("LeavingTime"), object.getString("Count"), "ProfilePic");
                                scheduled.setFrom(object.getString("StartFrom"));
                                scheduled.setTo(object.getString("Destination"));
                                scheduled.setStatus(object.getInt("CCount"));
                                if(scheduled.getStatus()!=0){
                                    scheduled.setSection(0);
                                    mList1.add(scheduled);
                                }else{
                                    scheduled.setSection(1);
                                    mList2.add(scheduled);
                                }
                                scheduled.setType("r");
                            }else{
                                Scheduled scheduled = new Scheduled(object.getString("Driverid"),object.getString("Userid"), object.getString("LeavingTime"), object.getString("Count"),"ProfilePic");
                                scheduled.setFrom(object.getString("StartFrom"));
                                scheduled.setTo(object.getString("Destination"));
                                scheduled.setStatus(object.getInt("CCount"));
                                if(scheduled.getStatus()!=0){
                                    scheduled.setSection(0);
                                    mList1.add(scheduled);
                                }else{
                                    scheduled.setSection(1);
                                    mList2.add(scheduled);
                                }
                                scheduled.setType("d");

                            }
                        }else{
                            Scheduled scheduled = new Scheduled(object.getString("Postid"),object.getString("Userid"), object.getString("LeavingTime"), object.getString("ProfilePic"));
                            scheduled.setFrom(object.getString("StartFrom"));
                            scheduled.setTo(object.getString("Destination"));
                            scheduled.setSection(0);
                            scheduled.setType(object.getString("Type"));
                            mList1.add(scheduled);
                        }

                    }

                    if(mList1.size() == 0&&mList2.size()==0){
                        view.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                    }else{
                        if(view.findViewById(R.id.empty).getVisibility()==View.VISIBLE){
                            view.findViewById(R.id.empty).setVisibility(View.INVISIBLE);
                        }
                        mAdapter.notifyDataChanged(mList);
                    }
                    boolean flag = false;
                    if(mList1.size()==0){
                        flag = true;
                        mAdapter.removeSection(0);
                    }if(mList2.size()==0){
                        mAdapter.removeSection(flag?0:1);
                    }
                    Log.v("list1", mList1.size()+"");
                    Log.v("list2", mList2.size()+"");
                    if(mSwipe.isRefreshing()){
                        mSwipe.setRefreshing(false);
                    }
                }catch (JSONException ex){
                    if(mSwipe.isRefreshing()){
                        mSwipe.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onError(String error) {
                mProgress.dismiss();
                if(getContext()!=null){
                    Toast.makeText(getContext(), "Network error", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
