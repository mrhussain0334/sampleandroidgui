package com.novelsol.hamrahi.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.novelsol.hamrahi.Adapters.BookingHistoryPagerAdapter;
import com.novelsol.hamrahi.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduledContainer extends Fragment {


    ViewPager mViewPager;
    TabLayout mTabs;
    BookingHistoryPagerAdapter mAdapter;
    Bundle intent;

    public ScheduledContainer() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_scheduled_container, container, false);
        mViewPager = (ViewPager)view.findViewById(R.id.viewpager);
        mTabs = (TabLayout)view.findViewById(R.id.tabs);
        mAdapter = new BookingHistoryPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mTabs.setupWithViewPager(mViewPager);
        intent = getArguments();
        switch (intent.getString("type")){
            case "rides":{
                getActivity().setTitle("My Rides");
                addPages("r");
                break;
            }case "drives":{
                getActivity().setTitle("My Drives");
                addPages("d");
                break;
            }
        }
        return view;
    }

    private void addPages(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("type", str);
        ScheduledRides schduledRides = new ScheduledRides();
        schduledRides.setArguments(bundle);
        mAdapter.addPage(schduledRides, "Scheduled");
        RidesHistory ridesHistory = new RidesHistory();
        mAdapter.addPage(ridesHistory, "History");
        mAdapter.notifyDataSetChanged();
    }

}
