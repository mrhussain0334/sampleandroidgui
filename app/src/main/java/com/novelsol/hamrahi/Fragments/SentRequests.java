package com.novelsol.hamrahi.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.novelsol.hamrahi.Adapters.AdapterSentRequests;
import com.novelsol.hamrahi.Driver.DriverConfirmation;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.Interested;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.MyTextView;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class SentRequests extends Fragment {


    RecyclerView mRecyclerView;
    AdapterSentRequests mAdapter;
    PreferencesHelper mHelper;
    ProgressDialogFragment mProgress;
    SwipeRefreshLayout mSwipe;


    ArrayList<Interested> mList = new ArrayList<>();
    View view;
    public SentRequests() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_sent_requests, container, false);
        mHelper = new PreferencesHelper(getContext());
        mSwipe = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayout);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callNetwork();
            }
        });
        mProgress = new ProgressDialogFragment();
        mProgress.setTextMessage("Loading..");
        mProgress.show(getActivity().getSupportFragmentManager(), "Progress");
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);
        mAdapter = new AdapterSentRequests(mList, getContext());
        mRecyclerView.setAdapter(mAdapter);
        callNetwork();
        this.view = view;
        return view;
    }

    void callNetwork(){
        HashMap<String, String> params = new HashMap<>();
        params.put("type", mHelper.getLoginAs()==0?"d": "r");
        params.put("userid", mHelper.getUserId());
        params.put("status", "I");
        HTTP.makeHttpPostRequest(getContext(), Utils.ALL_REQUESTS_REQUESTER, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                JSONArray array;
                Log.v("REQUESTS", response);
                mList.clear();
                mProgress.dismiss();
                try{
                    array = new JSONArray(response);
                    for(int i=0; i<array.length(); i++){
                        JSONObject obj = array.getJSONObject(i);
                        Interested interested = new Interested(obj.getString("Userid"), obj.getString("Name"), obj.getString("Phone"), obj.getString("ProfilePic"), obj.getString("Status"));
                        interested.setTo(obj.getString("Destination"));
                        interested.setFrom(obj.getString("StartFrom"));
                        interested.setTime(obj.getString("LeavingTime"));
                        interested.setBookingId(obj.getString("Bookingid"));
                        interested.setType("Type");
                        mList.add(interested);
                    }
                    if(mList.size()==0){
                        view.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                        ((MyTextView)view.findViewById(R.id.empty_txt)).setText("No requests");
                        mAdapter.notifyAll();
                    }else{
                        if(view.findViewById(R.id.empty).getVisibility()==View.VISIBLE){
                            view.findViewById(R.id.empty).setVisibility(View.INVISIBLE);
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                    if(mSwipe.isRefreshing()){
                        mSwipe.setRefreshing(false);
                    }
                    Log.v("Requests", response);
                }catch (JSONException ex){
                    if(mSwipe.isRefreshing()){
                        mSwipe.setRefreshing(false);
                    }
                    view.findViewById(R.id.empty).setVisibility(View.VISIBLE);
                    ((MyTextView)view.findViewById(R.id.empty_txt)).setText("No requests");
                }

            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
