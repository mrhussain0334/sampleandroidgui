package com.novelsol.hamrahi.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.loopj.android.http.TextHttpResponseHandler;
import com.novelsol.hamrahi.Adapters.VehicleAdapter;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.VehicleApi;
import com.novelsol.hamrahi.Model.Vehicle;
import com.novelsol.hamrahi.R;
import com.novelsol.hamrahi.ui.MyTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElementIterator;

/**
 * Created by Shahzaib on 9/8/2017.
 */

public class VehicleFragment extends Fragment implements VehicleAdapter.OnCardViewClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    public RecyclerView vehicleRecyclerView;
    private Vehicle vehicle;
    public static final int SHOW_DIALOG_FRAGMENT = 1;
    public static final int CREATE_DIALOG_FRAGMENT = 2;
    private ProgressDialog progressDialog;
    private PreferencesHelper mHelper;
    private static final String TAG = VehicleFragment.class.getSimpleName();
    FloatingActionButton mFab;
    View view;
    public VehicleFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_vehicle_activity, container, false);
        vehicleRecyclerView = (RecyclerView) v.findViewById(R.id.vehicleRecyclerView);
        mFab = (FloatingActionButton)v.findViewById(R.id.fab);
        getActivity().setTitle("Vehicles");
        if (vehicleRecyclerView != null) {
            List<Vehicle> list = new ArrayList<>();
            //String result = VehicleApi.GetVehicles(30);
            final VehicleAdapter adapter = new VehicleAdapter(list, getContext(), this);
            vehicleRecyclerView.setAdapter(adapter);
            mHelper = new PreferencesHelper(getContext());
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            vehicleRecyclerView.setLayoutManager(layoutManager);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(vehicleRecyclerView.getContext(),
                    layoutManager.getOrientation());
            //         vehicleRecyclerView.addItemDecoration(dividerItemDecoration);
            progressDialog = new ProgressDialog(getContext());
            Reload();
        }
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VehicleDialogFragment vd = new VehicleDialogFragment();
                vd.setTargetFragment(VehicleFragment.this,VehicleFragment.CREATE_DIALOG_FRAGMENT);
                vd.show(getFragmentManager(),"Add Vehicle");
            }
        });
        view = v;
        return v;
    }



    private void Reload() {
        VehicleApi.GetVehicle(Integer.parseInt(mHelper.getUserId()),new TextHttpResponseHandler(){
            @Override
            public void onStart() {
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage("Loading. Please wait...");
                progressDialog.setIndeterminate(true);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
            }
            @Override
            public void onFinish() {
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String response) {
                try {
                    ((VehicleAdapter) vehicleRecyclerView.getAdapter()).clear();

                    JSONArray newResponse = new JSONArray(response);
                    for (int i = 0; i < newResponse.length();i++)
                    {
                        JSONObject object = (JSONObject) newResponse.get(i);
                        Vehicle v = new Vehicle();
                        v.setVehicleId(object.getInt("Vehicleid"));
                        v.setCapacity(object.getInt("Capacity"));
                        v.setColor(object.getString("Color"));
                        v.setMake(object.getString("Make"));
                        v.setModel(object.getString("Model"));
                        v.setUserId(object.getInt("Userid"));
                        v.setRegNo(object.getString("RegNo"));
                        v.setVehicleType(object.getString("VehicleType"));
                        ((VehicleAdapter) vehicleRecyclerView.getAdapter()).add(v);

                    }
                    if(((VehicleAdapter) vehicleRecyclerView.getAdapter()).getItemCount() == 0){
                        Toast.makeText(getContext(), "No vehicles", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e) {
                    Log.e("Error",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if(responseString != null && responseString.length() > 0)
                    Log.e("Error", responseString);
                Toast.makeText(getContext(), "Error Getting Vehicle", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onCardViewClick(Vehicle v) {
        VehicleDialogFragment vd = VehicleDialogFragment.newInstance(v);
        vd.setTargetFragment(this, SHOW_DIALOG_FRAGMENT);
        vd.show(getFragmentManager(), "ShowVehicle");
    }

    @Override
    public void onCardViewLongClick(final Vehicle v) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        VehicleApi.DeleteVehicle(v.getVehicleId(), new TextHttpResponseHandler() {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                if(responseString != null && responseString.length() > 0)
                                    Log.e("Error", responseString);
                                Toast.makeText(getContext(), "Error Deleting Vehicle", Toast.LENGTH_SHORT).show();
                            }
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                Toast.makeText(getContext(), "Vehicle Deleted", Toast.LENGTH_SHORT).show();
                                Reload();
                            }
                        });
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Are you want to Delete your \"" + v.getVehicleType() + "\"?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case CREATE_DIALOG_FRAGMENT:
                final Vehicle v = (Vehicle) data.getSerializableExtra("vehicle");
                if (v == null) return;
                if (v.getVehicleId() == 0) {
                    VehicleApi.CreateVehicle(v, new TextHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String response) {
                            try {
                                JSONObject object = new JSONObject(response);
                                v.setVehicleId(object.getInt("Vehicleid"));
                                ((VehicleAdapter) vehicleRecyclerView.getAdapter()).add(v);
                                Toast.makeText(getContext(), "Vehicle Created", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                Log.e("Error", e.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            if(responseString != null && responseString.length() > 0)
                                Log.e("Error", responseString);
                            Toast.makeText(getContext(), "Error Creating Vehicle", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                break;
            case SHOW_DIALOG_FRAGMENT:

                try {
                    final Vehicle v2 = (Vehicle) data.getSerializableExtra("vehicle");
                    if (v2 == null) return;
                    if(v2.getVehicleId() == 0) return;
                    if(resultCode == Activity.RESULT_OK) {
                        VehicleApi.UpdateVehicle(v2, new TextHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                try {
                                    if (responseString.contains("Success")) {
                                        Toast.makeText(getContext(), "Vehicle Updated", Toast.LENGTH_SHORT).show();
                                        Reload();
                                    }
                                }
                                catch (Exception e)
                                {
                                    Log.e("Error",e.getMessage());
                                }

                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                if(responseString != null && responseString.length() > 0)
                                    Log.e("Error", responseString);
                                Toast.makeText(getContext(), "Error Updating Vehicle", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    else if(resultCode == Activity.RESULT_CANCELED) {
                        onCardViewLongClick(v2);
                    }
                }
                catch (Exception e) {
                    Log.e("Error",e.getMessage());
                }

                break;
        }
    }


}