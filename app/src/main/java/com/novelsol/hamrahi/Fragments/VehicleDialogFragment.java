package com.novelsol.hamrahi.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Model.Vehicle;
import com.novelsol.hamrahi.R;

import static android.view.View.GONE;

/**
 * A placeholder fragment containing a simple view.
 */
public class VehicleDialogFragment extends DialogFragment {

    private AppCompatImageButton btnCar, btnBike;
    private AppCompatImageButton btnSave,btnDelete;
    private TextView txtCar, txtBike;
    private EditText txtMake, txtModel, txtRegNo, txtColor;
    private AppCompatSpinner spCapacity;
    private boolean isCarSelected = true;
    private Vehicle vehicle;
    private int Options = 0;
    PreferencesHelper mHelper;
    RelativeLayout mRl;

    public VehicleDialogFragment() {
        vehicle = null;
    }

    public static VehicleDialogFragment newInstance(Vehicle v) {
        VehicleDialogFragment vd = new VehicleDialogFragment();
        vd.vehicle = v;
        return vd;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_vehicle, container, false);
        Initialize(v);
        Options = 1;
        mRl = (RelativeLayout)v.findViewById(R.id.rl);
        mHelper = new PreferencesHelper(getContext());
        getDialog().getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes(p);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        if (vehicle != null) {
            txtRegNo.setText(vehicle.getRegNo());
            txtColor.setText(vehicle.getColor());
            txtModel.setText(vehicle.getModel());
            txtMake.setText(vehicle.getMake());
            spCapacity.setSelection(vehicle.getCapacity());
            EditVehicle(false);
            if (vehicle.getVehicleType().equalsIgnoreCase("Car")) {
                SetSelected(btnCar, txtCar);
                SetNotSelected(btnBike, txtBike);
                isCarSelected = true;
            } else {
                SetSelected(btnBike, txtBike);
                SetNotSelected(btnCar, txtCar);
                isCarSelected = false;
                spCapacity.setVisibility(GONE);
            }
            Options = 2;
        }


        switch (Options) {
            case 1:
                //btnSave.setText("Save");
                break;
            case 2:
                //btnSave.setText("Edit");
                break;
            case 3:
                //btnSave.setText("Update");
                break;
        }
        return v;
    }

    private void Initialize(View v) {
        btnBike = (AppCompatImageButton) v.findViewById(R.id.btnBike);
        btnCar = (AppCompatImageButton) v.findViewById(R.id.btnCar);
        txtCar = (TextView) v.findViewById(R.id.txtCar);
        txtBike = (TextView) v.findViewById(R.id.txtBike);
        spCapacity = (AppCompatSpinner) v.findViewById(R.id.spCapacity);
        txtColor = (EditText) v.findViewById(R.id.txtColor);
        txtMake = (EditText) v.findViewById(R.id.txtMake);
        txtModel = (EditText) v.findViewById(R.id.txtModel);
        txtRegNo = (EditText) v.findViewById(R.id.txtRegNo);
        btnSave = (AppCompatImageButton) v.findViewById(R.id.btnSave);
        btnDelete = (AppCompatImageButton) v.findViewById(R.id.btnDelete);
        if (isCarSelected) {
            SetSelected(btnCar, txtCar);
            SetNotSelected(btnBike, txtBike);
        } else {
            SetSelected(btnBike, txtBike);
            SetNotSelected(btnCar, txtCar);
        }
        btnCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCarSelected) return;
                isCarSelected = true;
                SetSelected(btnCar, txtCar);
                SetNotSelected(btnBike, txtBike);
                spCapacity.setVisibility(View.VISIBLE);
            }
        });
        btnBike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCarSelected) return;
                isCarSelected = false;
                SetSelected(btnBike, txtBike);
                SetNotSelected(btnCar, txtCar);
                spCapacity.setVisibility(View.GONE);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (Options == 1) {
                        SaveVehicle();
                    } else if (Options == 2) {
                        EditVehicle(true);
                        Options++;
                        btnSave.setImageResource(R.drawable.ic_tick);
                        btnSave.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark), PorterDuff.Mode.MULTIPLY);
                        btnDelete.setImageResource(R.drawable.ic_close);
                    } else if (Options == 3) {
                        SaveVehicle();
                    }
                }
                catch (Exception e){
                    Log.e("Error",e.getMessage());
                }

            }
        });

        if(getTargetRequestCode() == VehicleFragment.SHOW_DIALOG_FRAGMENT) {
            btnBike.setClickable(false);
            btnCar.setClickable(false);
            btnSave.setImageResource(R.drawable.ic_edit);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Options == 3) {
                        dismiss();
                        return;
                    }
                    if (vehicle != null) {
                        Intent i = new Intent()
                                .putExtra("vehicle", vehicle);
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, i);
                        dismiss();
                    }
                }
            });
        }
        else {
            ViewGroup parent = (ViewGroup) btnDelete.getParent();
            if (parent != null) {
                parent.removeView(btnDelete);
            }
        }
    }
    private String CheckInputs() {
        try {
            if (txtRegNo.getText().length() > 15) {
                return "Enter Valid Registration No";
            }
            if (txtMake.getText().length() == 0) {
                return "Enter Valid Make";
            }
            if (txtModel.getText().length() == 0) {
                return "Enter Valid Model";
            }
            if (txtColor.getText().length() == 0) {
                return "Enter Valid Color";
            }
            if (spCapacity.getSelectedItemPosition() < 1 && isCarSelected) {
                return "Select Capacity";
            }
            return "Ok";
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return "Error";
    }
    private void SaveVehicle() throws Exception {
        String s = CheckInputs();
        if (!s.equalsIgnoreCase("ok")) {
            Toast.makeText(getContext(), s, BaseTransientBottomBar.LENGTH_SHORT).show();
            return;
        }
        if (vehicle == null) {
            vehicle = new Vehicle();
            vehicle.setUserId(Integer.parseInt(mHelper.getUserId()));
        }
        vehicle.setCapacity(spCapacity.getSelectedItemPosition());
        vehicle.setRegNo(txtRegNo.getText().toString());
        vehicle.setVehicleType(isCarSelected ? "Car" : "Bike");
        vehicle.setModel(txtModel.getText() + "");
        vehicle.setColor(txtColor.getText().toString());
        vehicle.setMake(txtMake.getText().toString());
        Intent i = new Intent()
                .putExtra("vehicle", vehicle);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
        dismiss();
    }
    private void EditVehicle(boolean isEnabled) {
        spCapacity.setEnabled(isEnabled);
        txtMake.setEnabled(isEnabled);
        txtRegNo.setEnabled(isEnabled);
        txtColor.setEnabled(isEnabled);
        txtModel.setEnabled(isEnabled);
    }
    private void SetSelected(AppCompatImageButton btn, TextView txt) {
        btn.clearColorFilter();
        txt.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
    }
    private void SetNotSelected(AppCompatImageButton btn, TextView txt) {
        btn.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.black), PorterDuff.Mode.MULTIPLY);
        txt.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
    }
}
