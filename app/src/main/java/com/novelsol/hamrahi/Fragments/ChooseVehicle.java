package com.novelsol.hamrahi.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.TextHttpResponseHandler;
import com.novelsol.hamrahi.Adapters.ChooseVehicleAdapter;
import com.novelsol.hamrahi.Adapters.VehicleAdapter;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.VehicleApi;
import com.novelsol.hamrahi.Model.Vehicle;
import com.novelsol.hamrahi.R;
import com.tuyenmonkey.mkloader.model.Line;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Shahzaib on 9/8/2017.
 */

public class ChooseVehicle extends DialogFragment {

    PreferencesHelper mHelper;
    Button mAddVehicle;
    ProgressBar mProgress;
    LinearLayout mLinearLayout;
    ListView mListView;
    ChooseVehicleAdapter mAdapter;
    ArrayList<Vehicle> mList = new ArrayList<>();
    Button mPost;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_choose_vehicle, container, false);
        mHelper = new PreferencesHelper(getContext());
        mPost = (Button)v.findViewById(R.id.post);
        getDialog().getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes(p);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        mProgress = (ProgressBar)v.findViewById(R.id.progress);
        mAddVehicle = (Button)v.findViewById(R.id.add_vehicle);
        mListView = (ListView)v.findViewById(R.id.list_view);
        mLinearLayout = (LinearLayout)v.findViewById(R.id.no_vehicle);
        mAdapter = new ChooseVehicleAdapter(mList);
        mListView.setAdapter(mAdapter);
        mAddVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddDialog();
            }
        });
        Reload();
        mPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return v;
    }


    private void Reload() {
        VehicleApi.GetVehicle(Integer.parseInt(mHelper.getUserId()),new TextHttpResponseHandler(){
            @Override
            public void onStart() {
                mPost.setVisibility(View.GONE);
            }
            @Override
            public void onFinish() {
                mProgress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String response) {
                try {
                    mList.clear();

                    JSONArray newResponse = new JSONArray(response);
                    for (int i = 0; i < newResponse.length();i++)
                    {
                        JSONObject object = (JSONObject) newResponse.get(i);
                        Vehicle v = new Vehicle();
                        v.setVehicleId(object.getInt("Vehicleid"));
                        v.setCapacity(object.getInt("Capacity"));
                        v.setColor(object.getString("Color"));
                        v.setMake(object.getString("Make"));
                        v.setModel(object.getString("Model"));
                        v.setUserId(object.getInt("Userid"));
                        v.setRegNo(object.getString("RegNo"));
                        v.setVehicleType(object.getString("VehicleType"));
                        mList.add(v);
                    }

                    if(mList.size()==0){
                        mLinearLayout.setVisibility(View.VISIBLE);
                        mPost.setVisibility(View.GONE);
                    }
                    mAdapter.notifyDataSetChanged();
                }
                catch (Exception e) {
                    Log.e("Error",e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if(responseString != null && responseString.length() > 0)
                    Log.e("Error", responseString);
                Toast.makeText(getContext(), "Error Getting Vehicle", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void showAddDialog(){
        VehicleDialogFragment vd = new VehicleDialogFragment();
        vd.setTargetFragment(new VehicleFragment(),VehicleFragment.CREATE_DIALOG_FRAGMENT);
        vd.show(getChildFragmentManager(),"Add Vehicle");
    }
}
