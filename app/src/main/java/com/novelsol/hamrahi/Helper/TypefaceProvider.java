package com.novelsol.hamrahi.Helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

/**
 * Created by Shahzaib on 5/28/2017.
 */

public class TypefaceProvider {

    private static final String LIGHT_FONT_LINK = "teen_light.ttf";
    private static final String REGULAR_FONT_LINK = "teen_regular.ttf";

    public static Typeface getLightTypeface(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), LIGHT_FONT_LINK);
        return typeface;
    }

    public static Typeface getRegularTypeface(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), REGULAR_FONT_LINK);
        return typeface;
    }


    public static Typeface getGothamLightTypeFace(Context context){
        return Typeface.createFromAsset(context.getAssets(), "GOTHAM-LIGHT.otf");
    }
    public static Typeface getGothamMediumTypeFace(Context context){
        return Typeface.createFromAsset(context.getAssets(), "GOTHAM-MEDIUM.TTF");
    }
    public static Typeface getGothamThinTypeFace(Context context){
        return Typeface.createFromAsset(context.getAssets(), "GOTHAM-THIN.ttf");
    }

    static public SpannableString getTypefaceSpan(Context context, String str){
        TypefaceSpan typefaceSpan = CustomTypefaceSpan.getInstance(getGothamLightTypeFace(context));
        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(typefaceSpan, 0, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }


}

@SuppressLint("ParcelCreator")
class CustomTypefaceSpan extends TypefaceSpan {
    private final Typeface mNewType;

    static CustomTypefaceSpan getInstance(Typeface typeface){
        return new CustomTypefaceSpan(typeface);
    }

    public CustomTypefaceSpan(Typeface type) {
        super("");
        mNewType = type;
    }

    public CustomTypefaceSpan(String family, Typeface type) {
        super(family);
        mNewType = type;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        applyCustomTypeFace(ds, mNewType);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        applyCustomTypeFace(paint, mNewType);
    }

    private void applyCustomTypeFace(Paint paint, Typeface tf) {
        int oldStyle;
        Typeface old = paint.getTypeface();
        if (old == null) {
            oldStyle = 0;
        } else {
            oldStyle = old.getStyle();
        }

        int fake = oldStyle & ~tf.getStyle();
        if ((fake & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(true);
        }

        if ((fake & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }

        paint.setTypeface(tf);
    }
}
