package com.novelsol.hamrahi.Helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.novelsol.hamrahi.ui.HamrahiApplication;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by ccn on 31/05/2017.
 */

public class LocationHelper implements LocationListener{

    static Location location;
    static ArrayList<Location> mLocations = new ArrayList<>();


    public static Location getLastKnownLocation(Context mContext) {
        LocationManager mLocationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (null != locationGPS) { GPSLocationTime = locationGPS.getTime(); }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }

        if ( 0 < GPSLocationTime - NetLocationTime ) {
            return locationGPS;
        }
        else {
            return locationNet;
        }
    }

    public Location getLocation(Context context, GoogleApiClient mGoogleClientApi){
            if (ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0 || ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleClientApi, this);
                LocationRequest mLocationRequest = LocationRequest.create();
                mLocationRequest.setInterval((long) (60*1000));
                mLocationRequest.setFastestInterval(400);
                mLocationRequest.setSmallestDisplacement(12.0f);
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleClientApi, mLocationRequest, this);
            }
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleClientApi);
        if(location!=null){
            mLocations.add(location);
        }else if(mLocations.size()>0){
            try{
                location = mLocations.get(mLocations.size());
            }catch (IndexOutOfBoundsException ex){
                location = mLocations.get(mLocations.size()-1);
            }
        }
        return location;
    }


    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    public static LocationHelper getInstance(){
        return new LocationHelper();
    }


}
