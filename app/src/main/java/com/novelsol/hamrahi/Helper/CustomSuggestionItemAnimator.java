package com.novelsol.hamrahi.Helper;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import com.mypopsy.widget.FloatingSearchView;
import com.mypopsy.widget.internal.ViewUtils;

/**
 * Created by ccn on 10/08/2017.
 */

public class CustomSuggestionItemAnimator extends BaseItemAnimator {

    private final static Interpolator INTERPOLATOR_ADD = new DecelerateInterpolator(3f);
    private final static Interpolator INTERPOLATOR_REMOVE = new AccelerateInterpolator(3f);

    private final FloatingSearchView mSearchView;

    public CustomSuggestionItemAnimator(FloatingSearchView searchView) {
        mSearchView = searchView;
        setAddDuration(150);
        setRemoveDuration(150);
    }

    @Override
    protected void preAnimateAdd(RecyclerView.ViewHolder holder) {
        if(!mSearchView.isActivated()) return;
        ViewCompat.setTranslationX(holder.itemView, 0);
        ViewCompat.setTranslationY(holder.itemView, -holder.itemView.getHeight());
        ViewCompat.setAlpha(holder.itemView, 0);
    }

    @Override
    protected ViewPropertyAnimatorCompat onAnimateAdd(RecyclerView.ViewHolder holder) {
        if(!mSearchView.isActivated()) return null;
        return ViewCompat.animate(holder.itemView)
                .translationY(0)
                .alpha(1)
                .setStartDelay((getAddDuration() / 2) * holder.getLayoutPosition())
                .setInterpolator(INTERPOLATOR_ADD);
    }

    @Override
    public boolean animateMove(RecyclerView.ViewHolder holder, int fromX, int fromY, int toX, int toY) {
        dispatchMoveFinished(holder);
        return false;
    }

    @Override
    protected ViewPropertyAnimatorCompat onAnimateRemove(RecyclerView.ViewHolder holder) {
        return ViewCompat.animate(holder.itemView)
                .alpha(0)
                .setStartDelay(0)
                .setInterpolator(INTERPOLATOR_REMOVE);
    }

}