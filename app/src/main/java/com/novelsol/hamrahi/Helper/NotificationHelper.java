package com.novelsol.hamrahi.Helper;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.novelsol.hamrahi.BroadcastReceivers.RequestAcceptReceiver;
import com.novelsol.hamrahi.BroadcastReceivers.RequestRejectReceiver;
import com.novelsol.hamrahi.Model.Notification;
import com.novelsol.hamrahi.R;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by ccn on 29/07/2017.
 */

public class NotificationHelper {

    Context mContext;
    Notification mNotification;

    public NotificationHelper(Context mContext){
        this.mContext = mContext;
    }





    public void setNotification(Notification notification){
        mNotification = notification;
    }

    public void show(){

        Intent rideAccepted = new Intent(mContext, RequestAcceptReceiver.class);
        Intent rideCancelled = new Intent(mContext, RequestRejectReceiver.class);


        rideAccepted.putExtra("notification", mNotification);
        rideCancelled.putExtra("notification", mNotification);

        PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(mContext, 0, rideCancelled, 0);
        PendingIntent acceptedPendingIntent = PendingIntent.getBroadcast(mContext, 1, rideAccepted, 0);

        RemoteViews notificationView = new RemoteViews(
                mContext.getPackageName(),
                R.layout.custom_notification
        );


        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.user)
                .setTicker("Notification from Hamrahi")
                .setOngoing(true)
                .setCustomBigContentView(notificationView);

        notificationView.setImageViewResource(R.id.imagenotileft,R.drawable.ic_cancel);

        notificationView.setTextViewText(R.id.title, mNotification.getTitle());
        notificationView.setTextViewText(R.id.text, mNotification.getMsg());

        notificationView.setOnClickPendingIntent(R.id.cancel, cancelPendingIntent);
        notificationView.setOnClickPendingIntent(R.id.accept, acceptedPendingIntent);

        NotificationManager notificationmanager = (NotificationManager)mContext. getSystemService(NOTIFICATION_SERVICE);
        notificationmanager.notify(Integer.valueOf(mNotification.getId()), builder.build());

    }
}
