package com.novelsol.hamrahi.Helper;

import com.google.android.gms.common.api.GoogleApiClient;
import com.novelsol.hamrahi.Services.LocationService;

import java.util.ArrayList;

/**
 * Created by Shahzaib on 7/16/2017.
 */

public class Utils {

    public static final String BASE = "http://ufmo.asia/RidesharingApi/public/";

    public static final String KEY = "qweasdzxc123";

    public static final String GET_AVAILABLE_RIDES = BASE+"driver/get/"+KEY;

    public static final String GET_REQUESTED_RIDES = BASE+"rider/get/"+KEY;

    public static final String POST_RIDER_REQUEST = BASE+"rider/create/"+KEY;

    public static final String POST_DRIVER_REQUEST = BASE+"driver/create/"+KEY;

    public static final String SIGNUP = BASE+"/user/create/"+KEY;

    public static final String POST_PHONE = BASE+"/user/phone/"+KEY;

    public static final String VERIFY_PHONE = BASE+"/user/verify/"+KEY;

    public static final String LOGIN = BASE+"/user/login/"+KEY;

    public static final String UPDATE_PROFILE = BASE+"/user/update/"+KEY;

    public static final String INTERESTED_PEOPLE = BASE+"/booking/get/"+KEY;

    public static final String TYPE_DRIVER = "d";

    public static final String TYPE_RIDER = "r";

    public static final String POST_FCM = BASE+"/user/fcm/"+KEY;

    public static final String POST_VERIFY_KEY = BASE+"/user/code/"+KEY;

    public static final String ACCEPT_REQUEST = BASE+"/booking/accept/"+KEY;

    public static final String CONFIRM = BASE+"/booking/confirm/"+KEY;

    public static final String REJECT = BASE+"/booking/reject/" + KEY;

    public static final String INTERESTED = BASE+"/booking/interested/"+KEY;

    public static final String UPDATE_PROFILE_PICTURE = BASE+"/user/pic/"+KEY;

    public static final String ALL_REQUESTS = BASE+"booking/allrequestowner/"+KEY;

    public static final String ALL_REQUESTS_REQUESTER = BASE+"booking/allrequestrequester/"+KEY;

    public static final String HISTORY = BASE+"history/get/"+KEY;

    public static GoogleApiClient mGoogleApiClient;

    public static final String GET_SCHEDULED = BASE+"booking/scheduled/"+KEY;

    public static final String POST_REVIEW = BASE+"review/create/"+KEY;

    public static final String GET_REVIEW = BASE+"review/get/"+KEY;

    public static final String CHANGE_PASSWORD = BASE+"user/changepassword/"+KEY;

    public static final String REJECT_REQUEST = BASE+"/booking/reject/"+KEY;

    public static final String CANCEL_BOOKING = BASE+"/booking/cancel/"+KEY;

    public static final String CANCEL_S_RIDER = BASE+"/rider/delete/"+KEY;

    public static final String CANCEL_S_DRIVER = BASE+"/driver/delete/"+KEY;


    public static final int PROFILE_MSG_SENT_REQUESTS = 0;
    public static final int PROFILE_MY_POSTS = 1;
    public static final int PROFILE_INTERESTED_ONLY = 2;
    public static final int POST_RIDE = 3;
    public static final int ADD_VEHICLE = 4;


    public static LocationService.LocationRequestCompleteCallback mCallback;

    public static final int OPEN_REQUEST = 1;
    public static final int OPEN_CONFIRMATION = 2;
    public static final int OPEN_SCHEDULED_RIDES = 3;
}
