package com.novelsol.hamrahi.Helper;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;
import com.loopj.android.http.SyncHttpClient;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Shahzaib on 7/16/2017.
 */

public class HTTP {

    public static final int GET = Request.Method.GET;
    public static final int POST = Request.Method.POST;

    public static void makeHttpGetRequest(Context mContext,String url, final RequestCompleteCallback callback){

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(mContext, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    callback.onRequestComplete(new String(responseBody, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    callback.onRequestComplete(new String(responseBody, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public static void makeHttpPostRequest(Context mContext,String url, final HashMap<String, String> params, final RequestCompleteCallback callback){
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(mContext, url, new RequestParams(params), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    callback.onRequestComplete(new String(responseBody, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.onRequestComplete(error.getLocalizedMessage());
            }
        });
    }

    public interface RequestCompleteCallback{
        void onRequestComplete(String response);
        void onError(String error);
    }
}
