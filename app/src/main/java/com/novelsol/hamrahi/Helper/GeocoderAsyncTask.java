package com.novelsol.hamrahi.Helper;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by ccn on 07/11/2017.
 */

public class GeocoderAsyncTask extends AsyncTask <LatLng, Void, String>{

    public Context context;
    ICallback mCallback;
    int requestType;

    @Override
    protected String doInBackground(LatLng... params) {

        LatLng mLatLng = params[0];

        List<Address> addresses = null;

        if(context != null){
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(
                        mLatLng.latitude,
                        mLatLng.longitude,
                        1);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            } catch (IllegalArgumentException illegalArgumentException) {
                illegalArgumentException.printStackTrace();
            }

            if (addresses == null || addresses.size()  == 0) {
                return null;
            } else {
                Address address = addresses.get(0);
                ArrayList<String> addressFragments = new ArrayList<String>();

                for(int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    addressFragments.add(address.getAddressLine(i));
                }
                return address.getAddressLine(0);
            }

        }
        return null;
    }

    public void setCallback(ICallback mCallback, int requestType){
        this.mCallback = mCallback;
        this.requestType = requestType;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(mCallback!=null){
            mCallback.onResult(s, requestType);
        }
    }

    public interface ICallback{
        void onResult(String address, int requestType);
    }
}
