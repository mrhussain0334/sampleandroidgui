package com.novelsol.hamrahi.Helper;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.novelsol.hamrahi.Model.Vehicle;

/**
 * Created by Shahzaib on 9/8/2017.
 */

public class VehicleApi{
    private static final String ROOT = "http://ufmo.asia/";
    private static final String KEY = "qweasdzxc123";
    private static final String CREATE = ROOT + "RidesharingApi/public/vehicle/create/" + KEY;
    private static final String UPDATE = ROOT + "RidesharingApi/public/vehicle/update/" + KEY;
    private static final String GET = ROOT + "RidesharingApi/public/vehicle/get/" + KEY;
    private static final String DELETE = ROOT + "RidesharingApi/public/vehicle/delete/" + KEY;
    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void CreateVehicle(Vehicle v, AsyncHttpResponseHandler handler)
    {
        try {
            RequestParams params = new RequestParams();
            params.add("userId",v.getUserId() + "");
            params.add("capacity",v.getCapacity() + "");
            params.add("color",v.getColor() + "");
            params.add("make",v.getMake() + "");
            params.add("model",v.getModel() + "");
            params.add("make",v.getMake() + "");
            params.add("regNo",v.getRegNo());
            params.add("vehicleType",v.getVehicleType() + "");
            client.post(CREATE,params,handler);
        }
        catch (Exception e)
        {
            Log.e("Error",e.getMessage());
        }
    }

    public static void UpdateVehicle(Vehicle v, AsyncHttpResponseHandler handler)
    {
        try {
            RequestParams params = new RequestParams();
            params.add("userid",v.getUserId() + "");
            params.add("id",v.getVehicleId() + "");
            params.add("capacity",v.getCapacity() + "");
            params.add("color",v.getColor() + "");
            params.add("make",v.getMake() + "");
            params.add("model",v.getModel() + "");
            params.add("make",v.getMake() + "");
            params.add("regNo",v.getRegNo());
            params.add("vehicleType",v.getVehicleType() + "");
            client.post(UPDATE,params,handler);
        }
        catch (Exception e)
        {
            Log.e("Error",e.getMessage());
        }
    }

    public static void GetVehicle(int id, AsyncHttpResponseHandler handler)
    {
        try {
            client.get(GET + "/" + id,null,handler);
        }
        catch (Exception e)
        {
            Log.e("Error",e.getMessage());
        }
    }

    public static void DeleteVehicle(int id, AsyncHttpResponseHandler handler)
    {
        try {
            RequestParams params = new RequestParams();
            params.add("id",id + "");
            client.post(DELETE,params,handler);
        }
        catch (Exception e)
        {
            Log.e("Error",e.getMessage());
        }
    }

}

