package com.novelsol.hamrahi.Helper;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by Shahzaib on 7/16/2017.
 */

public class PreferencesHelper {

    Context mContext;

    private static final String KEY_IS_LOGIN = "login";
    private static final String KEY_IS_CODE_RECEIVED = "code_verification";
    private static final String KEY_IS_FIRST_LAUNCH = "first_launch";
    private static final String KEY_USER_ID = "id";
    private static final String KEY_FB_ID = "fb_id";
    private static final String KEY_USER_NAME = "full_name";
    private static final String KEY_USER_PHONE = "phone";
    private static final String KEY_USER_IMG = "img";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_FCM_TOKEN = "token";
    private static final String KEY_LOGIN_AS = "loginas";


    public PreferencesHelper(Context mContext){
        this.mContext = mContext;
    }

    public boolean isLogin(){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(KEY_IS_LOGIN, false);
    }

    public void setLogin(boolean flag){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putBoolean(KEY_IS_LOGIN, flag).apply();
    }

    public boolean isCodeReceived(){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(KEY_IS_CODE_RECEIVED, false);
    }

    public void setCodeReceived(boolean flag){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putBoolean(KEY_IS_CODE_RECEIVED, flag).apply();
    }

    public String getUserName(){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(KEY_USER_NAME, "");
    }

    public void setUserName(String flag){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(KEY_USER_NAME, flag).apply();
    }

    public void setFirstLaunch(boolean key){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putBoolean(KEY_IS_FIRST_LAUNCH, key).apply();
    }

    public boolean isFirstLaunch(){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(KEY_IS_FIRST_LAUNCH, true);
    }

    public String getUserId() {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(KEY_USER_ID, "");
    }

    public String getFbId() {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(KEY_FB_ID, "");
    }

    public String getUserImg() {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(KEY_USER_IMG, "");
    }

    public String getUserPhone(){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(KEY_USER_PHONE, "");
    }

    public void getUserId(String key) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(KEY_USER_PHONE, key).apply();
    }

    public void setFbId(String key) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(KEY_FB_ID, key).apply();
    }

    public void setUserImg(String key) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(KEY_USER_IMG, key).apply();
    }

    public void setUserPhone(String key){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(KEY_USER_PHONE, key).apply();
    }

    public void setUserId (String key){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(KEY_USER_ID, key).apply();
    }


    public void setEmail(String key){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(KEY_EMAIL, key).apply();
    }
    public String getEmail(){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(KEY_EMAIL, "");
    }

    public void setPassword(String key){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(KEY_PASSWORD, key).apply();
    }
    public String getPassword(){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(KEY_PASSWORD, "");
    }

    public void setFCMTOken(String token){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(KEY_FCM_TOKEN, token).apply();
    }

    public String getFCMToken(){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(KEY_FCM_TOKEN, "");
    }

    public void setLoginAs(int key){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putInt(KEY_LOGIN_AS, key).apply();
    }

    public int getLoginAs(){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getInt(KEY_LOGIN_AS, 0);
    }
}
