package com.novelsol.hamrahi;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.LatLng;
import com.novelsol.hamrahi.Adapters.RidesAdapter;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;
import com.novelsol.hamrahi.Model.PostInfo;
import com.novelsol.hamrahi.Model.Ride;
import com.novelsol.hamrahi.ui.ProgressDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;



public class AvailableRideActivity extends AppCompatActivity implements HTTP.RequestCompleteCallback {

    ArrayList<Ride> mList = new ArrayList<>();
    LatLng mPlace;
    SwipeRefreshLayout mSwipe;
    RidesAdapter mAdapter;
    RecyclerView recyclerView;
    ProgressDialogFragment mProgress;
    PreferencesHelper mHelper;
    Button mPostRide;
    PostInfo info;
    ProgressDialogFragment dialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_available_rides);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        info = (PostInfo)intent.getSerializableExtra("post");
        mSwipe = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAvailableRides();
            }
        });
        mPlace = new LatLng(info.getEndLat(), info.getEndLon());

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewRides);
        mProgress = new ProgressDialogFragment();
        mHelper = new PreferencesHelper(this);
        mProgress.setTextMessage("Searching available Rides");
        mProgress.show(getSupportFragmentManager(), "F");
        mPostRide = (Button)findViewById(R.id.post_ride);

        mPostRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSuccess();
            }
        });

        mAdapter = new RidesAdapter(mList, this, Utils.TYPE_DRIVER);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setCB1(new RidesAdapter.CB1() {
            @Override
            public void onClick(int action) {
                switch (action){
                    case Utils.POST_RIDE:
                        showSuccess();
                        break;
                    case Utils.PROFILE_INTERESTED_ONLY:
                        getAvailableRides();
                        break;
                }

            }
        });
        if(recyclerView != null) {
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            getAvailableRides();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_post, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void showSuccess(){

        HashMap<String, String> params = new HashMap<>();

        params.put("userid", mHelper.getUserId());
        params.put("startfrom", info.getStartAddress());
        params.put("destination", info.getEndAddress());

        params.put("leavingtime", info.getLeavingTime());
        params.put("longstartfrom", String.valueOf(info.getStarLon()));
        params.put("latstartfrom", String.valueOf(info.getStartLat()));
        params.put("longdestination", String.valueOf(info.getEndLon()));
        params.put("latdestination", String.valueOf(info.getEndLat()));
        params.put("seat", "4");

        Log.v("Pick", "Leaving time : "+ info.getLeavingTime());

        HTTP.makeHttpPostRequest(this, Utils.POST_RIDER_REQUEST, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                dialog.dismiss();
                Log.d("Error",response);
                if(response.equalsIgnoreCase("success")){
                    Intent intent = new Intent();
                    intent.putExtra("data", Utils.PROFILE_MY_POSTS);
                    setResult(Utils.PROFILE_MY_POSTS, intent);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                dialog.dismiss();
            }
        });
        dialog = new ProgressDialogFragment();
        dialog.setTextMessage("Posting..");
        dialog.show(getSupportFragmentManager(), "P");
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            Intent intent = new Intent();
            intent.putExtra("data", Utils.PROFILE_MY_POSTS);
            setResult(Utils.PROFILE_MY_POSTS, intent);
            finish();            return true;
        }else if(id == R.id.post){
            showSuccess();
        }
        return super.onOptionsItemSelected(item);
    }

    void getAvailableRides(){
        HTTP.makeHttpGetRequest(AvailableRideActivity.this, Utils.GET_AVAILABLE_RIDES+"/"+mHelper.getUserId(), this);
    }

    @Override
    public void onRequestComplete(String response) {
        JSONArray mResArray;

        Log.v("Available drives", response);


        mProgress.dismiss();
        mList.clear();
        try{
            mResArray = new JSONArray(response);
            Log.v("Available drives", response);
            mList.clear();
            for(int i=0; i<mResArray.length(); i++){
                JSONObject object = mResArray.getJSONObject(i);
                Ride ride = new Ride();
                ride.setPostId(object.getString("Driverid"));
                ride.setDriverId(object.getString("Userid"));
                ride.setStart(object.getString("StartFrom"));
                ride.setDestination(object.getString("Destination"));
                ride.setStartLoc(new LatLng(Double.valueOf(object.getString("LatStartFrom")), Double.valueOf(object.getString("LongStartFrom"))));
                ride.setDestinationLoc(new LatLng(Double.valueOf(object.getString("LatDestination")), Double.valueOf(object.getString("LongDestination"))));
                ride.setLeavingTime(object.getString("LeavingTime"));
                ride.setInterestedPeople(object.getString("Count"));
                ride.setPicture(object.getString("ProfilePic"));
                ride.setPhone(object.getString("Phone"));
                ride.setEmail(object.getString("Email"));
                ride.setName(object.getString("Name"));
                ride.setVehicleNumber(object.getString("RegNo"));
                ride.setVehicleName(object.getString("Model"));
                ride.setSeats(object.getString("Capacity"));
                if(isInsideRadius(ride.getDestinationLoc())){
                    Log.v("Location2233", "Location Added");
                    mList.add(ride);
                }else{
                    Log.v("Location2233", "Location Cannot be Added");
                }
                Toast.makeText(getApplicationContext(), "Hi", Toast.LENGTH_LONG).show();

            }
            Log.d("AvailableRides", "Total available Rides : "+mList.size());
            if(mList.size() == 0){
                findViewById(R.id.empty).setVisibility(View.VISIBLE);
//                ((MyTextView)findViewById(R.id.empty_txt)).setText("No rides available");
            }else{
                findViewById(R.id.empty).setVisibility(View.INVISIBLE);
            }
            mAdapter.notifyDataSetChanged();
            if(mSwipe.isRefreshing()){
                mSwipe.setRefreshing(false);
            }
            Log.v("Available drives", response);
        }catch (JSONException ex){
            ex.printStackTrace();
            Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

            if(mSwipe.isRefreshing()){
                mSwipe.setRefreshing(false);
            }
        }
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, "Error while getting data from server", Toast.LENGTH_SHORT).show();
    }

    boolean isInsideRadius(LatLng end){

        Location location1 = new Location("Location1");
        location1.setLatitude(end.latitude);
        location1.setLongitude(end.longitude);

        Location location2 = new Location("Location2");
        location2.setLatitude(mPlace.latitude);
        location2.setLongitude(mPlace.longitude);

        double e = location1.distanceTo(location2)/1000;

        Log.v("DISTANCE", "Distance : "+e+" To"+ mPlace.latitude+", "+mPlace.longitude+" From : "+end.latitude+", "+end.longitude);

        if(e>25){
            return false;
        }else{
            return true;
        }
    }


}
