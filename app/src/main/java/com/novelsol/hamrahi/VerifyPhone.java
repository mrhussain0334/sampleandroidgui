package com.novelsol.hamrahi;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.novelsol.hamrahi.Helper.HTTP;
import com.novelsol.hamrahi.Helper.PreferencesHelper;
import com.novelsol.hamrahi.Helper.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Random;

public class VerifyPhone extends AppCompatActivity {

    EditText mPhone;
    Button mGo;

    String SENT = "SMS_SENT";
    String DELIVERED = "SMS_DELIVERED";

    String mSentCode;
    String mPhoneNumber;

    PreferencesHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        mPhone = (EditText)findViewById(R.id.phone);
        mGo = (Button)findViewById(R.id.go);
        mHelper = new PreferencesHelper(this);
        mGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPhone.getText().toString().length()>=11){
                    verifyPhoneFromServer(mPhone.getText().toString());
                }else{
                    Toast.makeText(VerifyPhone.this, "Please enter a valid phone number", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void verifyPhoneFromServer(String phone) {
        HashMap<String, String> params = new HashMap<>();;

        params.put("facebook_id", mHelper.getFbId());
        params.put("phonenumber", phone);

        HTTP.makeHttpPostRequest(this, Utils.POST_PHONE, params, new HTTP.RequestCompleteCallback() {
            @Override
            public void onRequestComplete(String response) {
                if(response.equalsIgnoreCase("success")){
                    sendCode();
                    Toast.makeText(VerifyPhone.this, "1" +response, Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(VerifyPhone.this, "2"+response, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(VerifyPhone.this, "Error ",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getCode(){
        Random random = new Random();
        return String.format("%04d",random.nextInt(10000));
    }

    private void sendCode() {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            String verificationCode = getCode();
            mSentCode = verificationCode;
            mPhoneNumber = mPhone.getText().toString();
            PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
            PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
            registerBroadCastReceivers();
            smsManager.sendTextMessage(mPhoneNumber, null, verificationCode+" is your verification code for Ride Sharing.", sentPI, deliveredPI);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void registerBroadCastReceivers(){

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Intent intent = new Intent(VerifyPhone.this, MobileVerificationActivity.class);
                        intent.putExtra("code", mSentCode);
                        intent.putExtra("phone", mPhoneNumber);
                        startActivity(intent);
                        Toast.makeText(getBaseContext(), "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {

                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {

                    SmsMessage[] messages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                    String number = messages[0].getOriginatingAddress();
                    StringBuilder sb = new StringBuilder();
                    for (SmsMessage message : messages) {
                        sb.append(message.getDisplayMessageBody());
                    }

                    String code = sb.toString().replaceAll("[^0-9]", "");

                    if(code.equals(mSentCode)){
                        mHelper.setCodeReceived(true);
                    }

                }
            }
        }, intentFilter);

    }


}
